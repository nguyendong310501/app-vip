// ignore_for_file: empty_catches, always_specify_types

import 'dart:convert';
import 'dart:io';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:rxdart/subjects.dart';


class FCMPushNotification {
  bool isFlutterLocalNotificationsInitialized = false;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  FCMPushNotificationDelegate? delegate;
  final BehaviorSubject<String> behaviorSubject = BehaviorSubject();

  FCMPushNotification({this.delegate});

  AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel',
    'High Importance Notifications',
    description: 'This channel is used for important notifications.',
    importance: Importance.high,
  );

  AndroidNotificationChannel channelLocal = const AndroidNotificationChannel(
    'high_importance_channel_local',
    'High Importance Notifications Local',
    description: 'This channel is used for important notifications local.',
    importance: Importance.high,
  );

  Future<void> requestPermission() async {
    await _init();
    // if (Platform.isIOS) {
    print('============FCM');

    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
    // }
  }

  Future<void> _createNotificationChannelAndroid() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
  }

  Future<void> _createNotificationChannelAndroidLocal() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channelLocal);
  }

  Future<void> _requestPermissionsIos() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          sound: true,
          alert: true,
          badge: true,
        );
  }

  Future<void> _setupFlutterNotifications() async {
    if (isFlutterLocalNotificationsInitialized) {
      return;
    }

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    await _createNotificationChannelAndroid();
    await _createNotificationChannelAndroidLocal();
    await _requestPermissionsIos();

    const IOSInitializationSettings iosSettings = IOSInitializationSettings();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher');

    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: iosSettings,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (String? payload) {
        try {
          final data = jsonDecode(payload ?? "");
          print("data: $payload");

          delegate?.onMessageOpenedApp(data);
        } catch (e) {}
      },
    );

    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) async {


      },
    );

    isFlutterLocalNotificationsInitialized = true;
  }

  bool checkTime(DateTime? createdAt) {
    if (createdAt == null) {
      return true;
    }
    DateTime now = DateTime.now();
    Duration difference = now.difference(createdAt);
    return difference.inMinutes < 1;
  }

  Future<String> _base64encodedImage(String url) async {
    final http.Response response = await http.get(Uri.parse(url));
    final String base64Data = base64Encode(response.bodyBytes);
    return base64Data;
  }

  Future<void> showPublicNotification(
    AndroidNotification? android,
    RemoteMessage message,
    RemoteNotification notification,
  ) async {
    final String? encodeImageBase64 = android?.imageUrl == null
        ? null
        : await _base64encodedImage(android?.imageUrl as String);

    final ByteArrayAndroidBitmap? largeIcon = encodeImageBase64 == null
        ? null
        : ByteArrayAndroidBitmap.fromBase64String(encodeImageBase64);

    final BigPictureStyleInformation? styleInformation =
        encodeImageBase64 == null
            ? null
            : BigPictureStyleInformation(
                ByteArrayAndroidBitmap.fromBase64String(encodeImageBase64),
                contentTitle: notification.title,
                htmlFormatContentTitle: true,
                hideExpandedLargeIcon: true,
                summaryText: notification.body,
                htmlFormatSummaryText: true,
              );

    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      channel.id,
      channel.name,
      channelDescription: channel.description,
      importance: Importance.max,
      priority: Priority.high,
      largeIcon: largeIcon,
      styleInformation: styleInformation,
      visibility: NotificationVisibility.public,
    );

    IOSNotificationDetails iOSNotificationDetails = IOSNotificationDetails(
      presentAlert: true,
      presentSound: true,
      presentBadge: true,
      badgeNumber: message.notification?.apple?.badge?.length,
    );

    if (Platform.isAndroid) {
      await flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: androidNotificationDetails,
          iOS: iOSNotificationDetails,
        ),
        payload: jsonEncode(message.data),
      );
    }
  }

  Future<void> cancelAllNotifications() =>
      flutterLocalNotificationsPlugin.cancelAll();

  void setupHandler() {
    try {
      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage? event) {
        if (event != null) {
          delegate?.onMessageOpenedApp(event.data);
        }
      });
    } catch (e) {}
  }

  Future<String?> getToken() async {
    // if (Platform.isIOS) {
    //   IosDeviceInfo iosDeviceInfo = await DeviceInfoHelper().readIosInfo();
    //   final Map<String, dynamic> data =
    //       DeviceInfoHelper().readIosDeviceInfo(iosDeviceInfo);
    //   if (!data['isPhysicalDevice']) return 'dahc.token.test.simulator';
    //   return FirebaseMessaging.instance.getAPNSToken();
    // }
    return FirebaseMessaging.instance.getToken();
  }

  Future<void> deleteToken() => FirebaseMessaging.instance.deleteToken();

  Future<void> _init() async {
    if (Firebase.apps.isEmpty) {
      // await Firebase.initializeApp(
      //   options: DefaultFirebaseOptions.currentPlatform,
      // );
    } else {
      await Firebase.initializeApp();
    }

    final String token = await getToken() ?? "";
    delegate?.onTokenRefresh(token);

    FirebaseMessaging.instance.onTokenRefresh.listen((String fcmToken) {
      delegate?.onTokenRefresh(fcmToken);
    });

    tz.initializeTimeZones();
    // tz.setLocalLocation(
      // tz.getLocation(
      //   await FlutterNativeTimezone.getLocalTimezone(),
      // ),
    // );

    await _setupFlutterNotifications();
  }

  void selectNotification(payload) {
    if (payload != null) {
      behaviorSubject.add(payload);
    }
  }
}

abstract class FCMPushNotificationDelegate {
  void onMessageOpenedApp(
    Map<String, dynamic> data,
  );
  void onTokenRefresh(String token);
}

class FCMPushNotificationAction extends FCMPushNotificationDelegate {
  @override
  void onMessageOpenedApp(Map<String, dynamic> data) async {}

  @override
  void onTokenRefresh(String token) {
    injector.get<AuthPreferencesRepository>().setDeviceToken(token);
  }
}
