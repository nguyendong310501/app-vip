import 'package:formz/formz.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/utilities/helpers/validator_helper/validator_helper.dart';

enum RepeatPasswordFieldSignUpValidationError {
  pure,
  empty,
  invalid,
  notMatch,
}

class Password {
  Password({
    required this.password,
    required this.repeatPassword,
  });
  final String password;
  final String repeatPassword;
}

extension RepeatPasswordValidationErrorExtension
    on RepeatPasswordFieldSignUpValidationError {
  String? get description {
    switch (this) {
      case RepeatPasswordFieldSignUpValidationError.pure:
        return null;
      case RepeatPasswordFieldSignUpValidationError.empty:
        return '비밀번호를 재입력해주세요.';
      case RepeatPasswordFieldSignUpValidationError.notMatch:
        return '비밀번호가 일치하지 않습니다.';
      case RepeatPasswordFieldSignUpValidationError.invalid:
        return '영문+숫자 조합 8자리 이상 입력해주세요.';
    }
  }
}

class RepeatPasswordFieldSignUp
    extends FormzInput<Password?, RepeatPasswordFieldSignUpValidationError> {
  RepeatPasswordFieldSignUp.pure() : super.pure(null);

  RepeatPasswordFieldSignUp.dirty([
    Password? value,
  ]) : super.dirty(value);

  final ValidatorHelper validatorHelper = injector.get<ValidatorHelper>();

  @override
  RepeatPasswordFieldSignUpValidationError? validator(Password? value) {
    if (value == null) {
      return RepeatPasswordFieldSignUpValidationError.pure;
    } else if (value.password.isEmpty) {
      return RepeatPasswordFieldSignUpValidationError.empty;
    } else {
      if (value.password != value.repeatPassword) {
        return RepeatPasswordFieldSignUpValidationError.notMatch;
      }

      if (validatorHelper.isNumericLengthPasswordValid(value.repeatPassword)) {
        return null;
      } else {
        return RepeatPasswordFieldSignUpValidationError.invalid;
      }
    }
  }
}
