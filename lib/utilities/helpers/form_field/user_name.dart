import 'package:formz/formz.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/utilities/helpers/validator_helper/validator_helper.dart';

enum UserNameValidationError {
  pure,
  empty,
  invalid
}

extension UserNameValidationErrorExtension on UserNameValidationError {
  String? get description {
    switch (this) {
      case UserNameValidationError.pure:
        return null;
      case UserNameValidationError.empty:
        return '이름을 입력해주세요.';
      case UserNameValidationError.invalid:
        return '이름은 최소 2자 이상 입력해주세요.';
    }
  }
}

class UserName extends FormzInput<String?, UserNameValidationError> {
  UserName.pure() : super.pure(null);

  UserName.dirty([String value = '']) : super.dirty(value);

  final ValidatorHelper validatorHelper = injector.get<ValidatorHelper>();

  @override
  UserNameValidationError? validator(String? value) {
    if (value == null) {
      return UserNameValidationError.pure;
    } else if (value.isEmpty) {
      return UserNameValidationError.empty;
    } else if(value.length < 2){
      return UserNameValidationError.invalid;

    }else

    {
      return null;
    }
  }
}
