import 'package:formz/formz.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/utilities/helpers/validator_helper/validator_helper.dart';

enum PasswordSignUpValidationError {
  pure,
  empty,
  invalid,
}

extension PasswordSignUpValidationErrorExtension
    on PasswordSignUpValidationError {
  String? get description {
    switch (this) {
      case PasswordSignUpValidationError.pure:
        return null;
      case PasswordSignUpValidationError.empty:
        return ' Please enter a password.';
      case PasswordSignUpValidationError.invalid:
        return 'Please enter at least 8 characters with a combination of letters and numbers.';
    }
  }
}

class PasswordSignUp
    extends FormzInput<String?, PasswordSignUpValidationError> {
  PasswordSignUp.pure() : super.pure(null);

  PasswordSignUp.dirty([String value = '']) : super.dirty(value);

  final ValidatorHelper validatorHelper = injector.get<ValidatorHelper>();

  @override
  PasswordSignUpValidationError? validator(String? value) {
    if (value == null) {
      return PasswordSignUpValidationError.pure;
    } else if (value.isEmpty) {
      return PasswordSignUpValidationError.empty;
    } else if (validatorHelper.isNumericLengthPasswordValid(value)) {
      return null;
    } else {
      return PasswordSignUpValidationError.invalid;
    }
  }
}
