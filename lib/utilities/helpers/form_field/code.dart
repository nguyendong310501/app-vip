import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/utilities/helpers/validator_helper/validator_helper.dart';
import 'package:formz/formz.dart';

enum CodeValidationError {
  pure,
  empty,
  invalid,

}

extension CodeValidationErrorExtension on CodeValidationError {
  String? get description {
    switch (this) {
      case CodeValidationError.pure:
        return null;
      case CodeValidationError.empty:
        return 'Invalid code number';
      case CodeValidationError.invalid:
        return 'Invalid code number';
    }
  }
}

class Code extends FormzInput<String?, CodeValidationError> {
  Code.pure() : super.pure(null);

  Code.dirty([String value = '']) : super.dirty(value);

  final ValidatorHelper validatorHelper = injector.get<ValidatorHelper>();

  @override
  CodeValidationError? validator(String? value) {
    if (value == null) {
      return CodeValidationError.pure;
    } else if (value.isEmpty) {
      return CodeValidationError.empty;
    } else if (validatorHelper.isNumericLengthCodeValid(value)) {
      return null;
    } else {
      return CodeValidationError.invalid;
    }
  }
}
