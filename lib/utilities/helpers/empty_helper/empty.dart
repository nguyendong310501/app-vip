import 'package:flutter/material.dart';
import '../../../resources/app_colors.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorsLight.background,
      child: Padding(
        padding: const EdgeInsets.only(top: 50.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.hourglass_empty_outlined,
                color: context.colors.backgroundPrimary,
                size: 30,
              ),
              const SizedBox(height: 4),
              Text('정보 없습니다',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colorsLight.textSecondary,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
