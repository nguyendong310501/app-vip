import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiConstants {
  ApiConstants._();
  // Base Url
  static final String apiBaseUrl = dotenv.env['BASE_URL'] ?? "";
  static final String apiBaseUrlUpload = dotenv.env['BASE_URL_UPLOAD'] ?? "";
  // User
  static const String userLogin = '/api/auth/login';
  static const String infoUser = '/api/users/me';
  static const String userRegister = '/api/register';
  static const String userForgot = '';
  static const String employeeStatus = '/api/employee/status/{id}';
  static const String sector = '';

  static const String userLogout = '';

  static const String signUp = '';

  static const String verifyCode = '';

  static const String resendCode = '';

  static const String upTokenDevices = '/api/firebase/subscribe';

  static const String authLogout = '/api/auth/logout';

  static const String uploadImage = '/api/s3';

  static const String deleteImage = '';


  static const String loginKakao = '/api/auth/kakao/login';

  static const String loginNaver = '/api/auth/naver/login';

  static const List<String> nonAuthenticatedPaths = <String>[
    ApiConstants.userLogin,
  ];
}
