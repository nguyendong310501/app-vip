import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_oke_vip/utilities/helpers/load_more/base_load_more_page_mixin.dart';
import 'package:kakao_flutter_sdk/kakao_flutter_sdk.dart';
import 'package:loadmore/loadmore.dart';
import '../injection/dependencies.dart';
import '../presentation/app/app.dart';
import '../utilities/helpers/app_helper/app_flavor_helper.dart';


final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

Future<void> mainCommon(AppFlavor flavor) async {
  void configImageCached() {
    PaintingBinding.instance.imageCache.maximumSizeBytes = 0;
    PaintingBinding.instance.imageCache.maximumSizeBytes = 1000 << 20;
  }

  WidgetsFlutterBinding.ensureInitialized();
  KakaoSdk.init(
    nativeAppKey: 'c30d26b486ed531f5a5bbfd1e5f2efdc',
    javaScriptAppKey: '871454076e7c556a48221cef356920b9',
  );


  await DependencyManager.inject(flavor);
  await flavor.setup();



  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  configImageCached();
  LoadMore.buildDelegate = () => BaseLoadMorePage();

  runApp(DevicePreview(
    enabled: kIsWeb & !kReleaseMode,
    builder: (BuildContext context) => const App(),
  ));
}
