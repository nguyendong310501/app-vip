class AppImages {
  AppImages._private();

  static const String _path = 'assets/images';

  // background
  static const String imageSplash = '$_path/img_rectangle.png';
  static const String imageLogo = '$_path/logo.png';
  static const String backgroundBase = '$_path/background_base.png';
  static const String imgEmpty = '$_path/ic_empty.png';
  static const String imgRectangle = '$_path/img_rectangle.png';
  static const String imgEllipse = '$_path/img_ellipse.png';
  static const String heroText = '$_path/hero-text.png';
  static const String upload = '$_path/upload.png';

}
