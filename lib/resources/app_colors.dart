import 'package:flutter/material.dart';

class AppColors {
  const AppColors(
      {
      //background
      required this.background,
      required this.backgroundPrimary,
      required this.backgroundSecondary,
      required this.backgroundThirdary,
      required this.backgroundInput,
      required this.backgroundGoldenrod,
      required this.backgroundPaleRose,
      required this.backgroundLinen,
      required this.backgroundIsLandSpice,
      required this.backgroundTara,
      required this.backgroundSolitude,
      required this.selectedBackground,
      required this.unselectedBackground,
      required this.backgroundWhite,
      required this.backgroundBlue,
      required this.backgroundButon,
      required this.backgroundGreen,
      required this.backgroundLine,

      //text
      required this.title,
      required this.label,
      required this.labelSecondary,
      required this.textPrimary,
      required this.textSecondary,
      required this.textTertiary,
      required this.textDisable,
      required this.textCerulean,
      required this.textGolden,
      required this.textLimeGreen,
      required this.textHarvestGold,
      required this.textLimeGreenStatus,
      required this.textCuriousBlue,

      //input
      required this.border,
      required this.borderInput,
      required this.borderInputCode,

      //common
      required this.error,
      required this.required,
      required this.mistyQuartz,
      required this.lightcCharcoal,
      required this.lightGray,
      required this.black,
      required this.lavenderPurple,
      required this.goldenrod,
      required this.fireEngineRed,
      required this.limeGreen,
      required this.gunmetal,
      required this.cornflowerBlue,
      required this.cloudGray,
      required this.creamy,
      required this.hFFB700,
      required this.hA0A0A0,
      required this.h37DC00,
      required this.hFEE500,
      required this.h133145,
      required this.hFF7979,
      required this.hC3C3C3,
      required this.hF4F4F4,
      required this.h090A0A,
      required this.hE3E5E5,
      required this.h011E32,
      required this.h56DE7A,
      required this.hCCD4D9,
      required this.hF6FBFF,
      required this.hD9D9D9,
      required this.h969595,
      required this.h585858,
      required this.hCDCFD0,
      required this.hE1E1E1,
      required this.h3081FF});

  final Color background;
  final Color backgroundPrimary;
  final Color backgroundSecondary;
  final Color backgroundThirdary;
  final Color backgroundInput;
  final Color backgroundGoldenrod;
  final Color backgroundPaleRose;
  final Color backgroundLinen;
  final Color backgroundIsLandSpice;
  final Color backgroundTara;
  final Color backgroundSolitude;
  final Color selectedBackground;
  final Color unselectedBackground;
  final Color backgroundWhite;
  final Color backgroundBlue;
  final Color backgroundButon;
  final Color backgroundGreen;
  final Color backgroundLine;
  final Color h133145;

  //text
  final Color label;
  final Color labelSecondary;
  final Color textPrimary;
  final Color textSecondary;
  final Color textTertiary;
  final Color title;
  final Color textDisable;
  final Color textCerulean;
  final Color textGolden;
  final Color textLimeGreen;
  final Color textHarvestGold;
  final Color textLimeGreenStatus;
  final Color textCuriousBlue;
  final Color hA0A0A0;
  final Color h3081FF;
  //input
  final Color border;
  final Color borderInput;
  final Color borderInputCode;

  //common
  final Color error;
  final Color required;
  final Color mistyQuartz;
  final Color lightcCharcoal;
  final Color lightGray;
  final Color black;
  final Color lavenderPurple;
  final Color goldenrod;
  final Color fireEngineRed;
  final Color limeGreen;
  final Color gunmetal;
  final Color cornflowerBlue;
  final Color cloudGray;
  final Color creamy;

  final Color hFFB700;
  final Color h37DC00;
  final Color hFEE500;
  final Color hFF7979;
  final Color hF4F4F4;
  final Color hC3C3C3;
  final Color h090A0A;
  final Color hE3E5E5;
  final Color h56DE7A;
  final Color hF6FBFF;
  final Color hD9D9D9;
  final Color h969595;
  final Color h011E32;
  final Color hCCD4D9;
  final Color h585858;
  final Color hCDCFD0;
  final Color hE1E1E1;
}

const AppColors colorsLight = AppColors(
  //background
  background: Color(0xFFF6F6F6),
  backgroundPrimary: Color(0xFFA7302A),
  backgroundSecondary: Color(0xFFFAFAFA),
  backgroundThirdary: Color(0xFFEBBE49),
  backgroundInput: Color(0xFFFBFBFB),
  backgroundGoldenrod: Color(0xFFF4D462),
  backgroundPaleRose: Color(0xFFEFEFEF),
  backgroundLinen: Color(0xFFFBF8F0),
  textGolden: Color(0xFFFFC843),
  backgroundIsLandSpice: Color(0xFFFAEFDC),
  backgroundTara: Color(0xFFE0E0E0),
  backgroundSolitude: Color(0xFFE0ECFA),
  backgroundWhite: Color(0xFFFFFFFF),
  selectedBackground: Color(0xFFEBBE49),
  unselectedBackground: Color(0xFFEDEDED),
  backgroundBlue: Color(0xFFF3F8FF),
  backgroundButon: Color(0xFF005FC7),
  backgroundGreen: Color(0xFF29B014),
  backgroundLine: Color(0xFFC7C7C7),
  h133145: Color(0xFF133145),
  hFF7979: Color(0xFFFF7979),
  hC3C3C3: Color(0xFFC3C3C3),
  hF4F4F4: Color(0xFFF4F4F4),
  h090A0A: Color(0xFF090A0A),
  hE3E5E5: Color(0xFFE3E5E5),
  h011E32: Color(0xFF011E32),
  h56DE7A: Color(0xFF56DE7A),
  hCCD4D9: Color(0xFFCCD4D9),
  hF6FBFF: Color(0xFFF6FBFF),
  hD9D9D9: Color(0xFFD9D9D9),
  hCDCFD0: Color(0xFFCDCFD0),
  hE1E1E1: Color(0xFFE1E1E1),
  h3081FF: Color(0xFF3081FF),

  //text
  label: Color(0xFF211F20),
  labelSecondary: Color(0xFFFFFFFF),
  textPrimary: Color(0xFF444444),
  textSecondary: Color(0xFFA7302A),
  textTertiary: Color(0xFFFA8787),
  title: Color(0xFF272727),
  textDisable: Color(0xFFA2A2A2),
  textCerulean: Color(0xFF28A2D4),
  textLimeGreen: Color(0xFF11AF22),
  textHarvestGold: Color(0xFFF1C267),
  textLimeGreenStatus: Color(0xFF3BC92F),
  textCuriousBlue: Color(0xFF3B82D6),
  hA0A0A0: Color(0xFFA0A0A0),
  hFEE500: Color(0xFFFEE500),
  h37DC00: Color(0xFF37DC00),
  h969595: Color(0xFF969595),
  h585858: Color(0xFF585858),

  //input
  border: Color(0xFFE2E2E2),
  borderInput: Color(0xFFE2E2E2),
  borderInputCode: Color(0xFFF6F6F6),

  //common
  error: Color(0xFFF21D1D),
  required: Color(0xFFF21D1D),
  mistyQuartz: Color(0xFFC8C7C7),
  lightcCharcoal: Color(0xFFC8C7C7),
  lightGray: Color(0xFFCCCCCC),
  black: Color(0xFF000000),
  lavenderPurple: Color(0xFFB476F2),
  goldenrod: Color(0xFFF1C267),
  fireEngineRed: Color(0xFFE33535),
  limeGreen: Color(0xFF3BC92F),
  gunmetal: Color(0xFF878687),
  cornflowerBlue: Color(0xFF3B82D6),
  cloudGray: Color(0xFFEDEDED),
  creamy: Color(0xFFFDF8E7),
  hFFB700: Color(0xFFFFB700),
);

const AppColors colorsDark = AppColors(
  background: Color(0xFFF6F6F6),
  backgroundPrimary: Color(0xFFA7302A),
  backgroundSecondary: Color(0xFFFAFAFA),
  backgroundThirdary: Color(0xFFEAF6FB),
  backgroundInput: Color(0xFFFBFBFB),
  backgroundGoldenrod: Color(0xFFF4D462),
  backgroundPaleRose: Color(0xFFEFEFEF),
  backgroundLinen: Color(0xFFFBF8F0),
  textGolden: Color(0xFFFFC843),
  textLimeGreen: Color(0xFF11AF22),
  backgroundIsLandSpice: Color(0xFFFAEFDC),
  backgroundTara: Color(0xFFE0E0E0),
  backgroundSolitude: Color(0xFFE0ECFA),
  backgroundWhite: Color(0xFFFFFFFF),
  selectedBackground: Color(0xFFEBBE49),
  unselectedBackground: Color(0xFFEDEDED),
  backgroundBlue: Color(0xFF6BA1E2),
  backgroundButon: Color(0xFF005FC7),
  backgroundGreen: Color(0xFF29B014),
  backgroundLine: Color(0xFFC7C7C7),
  h133145: Color(0xFF133145),
  hC3C3C3: Color(0xFFC3C3C3),
  hF4F4F4: Color(0xFFF4F4F4),
  h090A0A: Color(0xFF090A0A),
  hE3E5E5: Color(0xFFE3E5E5),
  h011E32: Color(0xFF011E32),
  h56DE7A: Color(0xFF56DE7A),
  hCCD4D9: Color(0xFFCCD4D9),
  hF6FBFF: Color(0xFFF6FBFF),
  hD9D9D9: Color(0xFFD9D9D9),
  h969595: Color(0xFF969595),
  h585858: Color(0xFF585858),
  hCDCFD0: Color(0xFFCDCFD0),
  hE1E1E1: Color(0xFFE1E1E1),
  h3081FF: Color(0xFF3081FF),
  
  //text
  label: Color(0xFF211F20),
  labelSecondary: Color(0xFFFFFFFF),
  textPrimary: Color(0xFF878687),
  textSecondary: Color(0xFFA7302A),
  textTertiary: Color(0xFFFA8787),
  title: Color(0xFF090A0A),
  textDisable: Color(0xFFA2A2A2),
  textCerulean: Color(0xFF28A2D4),
  textHarvestGold: Color(0xFFF1C267),
  textLimeGreenStatus: Color(0xFF3BC92F),
  textCuriousBlue: Color(0xFF3B82D6),
  hA0A0A0: Color(0xFFA0A0A0),
  hFEE500: Color(0xFFFEE500),
  h37DC00: Color(0xFF37DC00),

  //input
  border: Color(0xFFE8EAEC),
  borderInput: Color(0xFFA0A0A0),
  borderInputCode: Color(0xFFF6F6F6),

  //common
  error: Color(0xFFF21D1D),
  required: Color(0xFFF21D1D),
  mistyQuartz: Color(0xFFC8C7C7),
  lightcCharcoal: Color(0xFFC8C7C7),
  lightGray: Color(0xFFCCCCCC),
  black: Color(0xFF000000),
  lavenderPurple: Color(0xFFB476F2),
  goldenrod: Color(0xFFF1C267),
  fireEngineRed: Color(0xFFE33535),
  limeGreen: Color(0xFF3BC92F),
  gunmetal: Color(0xFF878687),
  cornflowerBlue: Color(0xFF3B82D6),
  cloudGray: Color(0xFFEDEDED),
  creamy: Color(0xFFFDF8E7),
  hFFB700: Color(0xFFFFB700),
  hFF7979: Color(0xFFFF7979),
);

extension AppColorsExtension on BuildContext {
  AppColors get colors {
    final Brightness brightness = Theme.of(this).brightness;
    switch (brightness) {
      case Brightness.light:
        return colorsLight;
      case Brightness.dark:
        return colorsDark;
    }
  }
}
