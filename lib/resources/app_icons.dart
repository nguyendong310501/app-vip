class AppIcons {
  AppIcons._private();

  static const String _iconsPath = 'assets/icons';

  static const String icCheckCircle = '$_iconsPath/ic_check_circle.svg';
  static const String icHistoryAction =
      '$_iconsPath/ic_check_circle_active.svg';
  static const String icHomeActive = '$_iconsPath/ic_home_active.svg';
  static const String icHome = '$_iconsPath/ic_home.svg';
  static const String icLogin = '$_iconsPath/ic_login.svg';
  static const String icUserAction = '$_iconsPath/ic_user_active.svg';
  static const String icUser = '$_iconsPath/ic_user.svg';
  static const String icChevronLeft = '$_iconsPath/ic_chevron_left.png';
  static const String icChevronDown = '$_iconsPath/ic_arrow_downs.svg';
  static const String icChevronRightGray =
      '$_iconsPath/ic_chevron_right_gray.png';
  static const String icChevronRight = '$_iconsPath/ic_chevron_right.png';
  static const String icEmpty = '$_iconsPath/ic_empty.png';
  static const String icXCircleFill = '$_iconsPath/ic_x_circle_fill.svg';
  static const String icSearchBar = '$_iconsPath/ic_search_bar.svg';
  static const String icSearchResult = '$_iconsPath/ic_search_result.svg';
  static const String icSearch = '$_iconsPath/ic_search.svg';
  static const String icArrowLeft = '$_iconsPath/ic_arrow_left.png';
  static const String icCheck = '$_iconsPath/ic_check.png';
  static const String icLogout = '$_iconsPath/ic_log_out.svg';
  static const String icClose = '$_iconsPath/ic_close.svg';
  static const String icChevronDows = '$_iconsPath/ic_arrow_down.svg';
  static const String icChevronLeftSvg = '$_iconsPath/ic_arrow_left.svg';
  static const String icClock = '$_iconsPath/ic_clock.svg';
  static const String icAddress = '$_iconsPath/ic_address.svg';
  static const String icPhone = '$_iconsPath/ic_phone.svg';
  static const String icChat = '$_iconsPath/ic_box_chat.svg';
  static const String icEyeOff = '$_iconsPath/ic_eye_off.svg';
  static const String icCalendar = '$_iconsPath/ic_calendar.svg';
  static const String icPubCallabInactive =
      '$_iconsPath/ic_new_feed_tab_inactive.svg';
  static const String icPubCallTabActive =
      '$_iconsPath/ic_new_feed_tab_active.svg';
  static const String icImage = '$_iconsPath/ic_image.svg';
  static const String icKakaotalk =
      '$_iconsPath/ic_kakaotalk.svg';
  static const String icNaver = '$_iconsPath/ic_naver.svg';
  static const String icCloses = '$_iconsPath/ic_closes.svg';

  static const String icSplash = '$_iconsPath/ic_logo_splash.svg';

}
