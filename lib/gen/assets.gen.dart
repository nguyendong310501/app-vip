/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/ic_address.svg
  SvgGenImage get icAddress => const SvgGenImage('assets/icons/ic_address.svg');

  /// File path: assets/icons/ic_arrow_down.svg
  SvgGenImage get icArrowDown =>
      const SvgGenImage('assets/icons/ic_arrow_down.svg');

  /// File path: assets/icons/ic_arrow_downs.svg
  SvgGenImage get icArrowDowns =>
      const SvgGenImage('assets/icons/ic_arrow_downs.svg');

  /// File path: assets/icons/ic_arrow_left.svg
  SvgGenImage get icArrowLeft =>
      const SvgGenImage('assets/icons/ic_arrow_left.svg');

  /// File path: assets/icons/ic_bell.svg
  SvgGenImage get icBell => const SvgGenImage('assets/icons/ic_bell.svg');

  /// File path: assets/icons/ic_box_chat.svg
  SvgGenImage get icBoxChat =>
      const SvgGenImage('assets/icons/ic_box_chat.svg');

  /// File path: assets/icons/ic_calendar.svg
  SvgGenImage get icCalendar =>
      const SvgGenImage('assets/icons/ic_calendar.svg');

  /// File path: assets/icons/ic_chat.svg
  SvgGenImage get icChat => const SvgGenImage('assets/icons/ic_chat.svg');

  /// File path: assets/icons/ic_chat_active.svg
  SvgGenImage get icChatActive =>
      const SvgGenImage('assets/icons/ic_chat_active.svg');

  /// File path: assets/icons/ic_check_circle.svg
  SvgGenImage get icCheckCircle =>
      const SvgGenImage('assets/icons/ic_check_circle.svg');

  /// File path: assets/icons/ic_check_circle_active.svg
  SvgGenImage get icCheckCircleActive =>
      const SvgGenImage('assets/icons/ic_check_circle_active.svg');

  /// File path: assets/icons/ic_chevron_down.svg
  SvgGenImage get icChevronDown =>
      const SvgGenImage('assets/icons/ic_chevron_down.svg');

  /// File path: assets/icons/ic_clock.svg
  SvgGenImage get icClock => const SvgGenImage('assets/icons/ic_clock.svg');

  /// File path: assets/icons/ic_close.svg
  SvgGenImage get icClose => const SvgGenImage('assets/icons/ic_close.svg');

  /// File path: assets/icons/ic_closes.svg
  SvgGenImage get icCloses => const SvgGenImage('assets/icons/ic_closes.svg');

  /// File path: assets/icons/ic_community.svg
  SvgGenImage get icCommunity =>
      const SvgGenImage('assets/icons/ic_community.svg');

  /// File path: assets/icons/ic_community_active.svg
  SvgGenImage get icCommunityActive =>
      const SvgGenImage('assets/icons/ic_community_active.svg');

  /// File path: assets/icons/ic_eye_off.svg
  SvgGenImage get icEyeOff => const SvgGenImage('assets/icons/ic_eye_off.svg');

  /// File path: assets/icons/ic_home.svg
  SvgGenImage get icHome => const SvgGenImage('assets/icons/ic_home.svg');

  /// File path: assets/icons/ic_home_active.svg
  SvgGenImage get icHomeActive =>
      const SvgGenImage('assets/icons/ic_home_active.svg');

  /// File path: assets/icons/ic_image.svg
  SvgGenImage get icImage => const SvgGenImage('assets/icons/ic_image.svg');

  /// File path: assets/icons/ic_inbox.svg
  SvgGenImage get icInbox => const SvgGenImage('assets/icons/ic_inbox.svg');

  /// File path: assets/icons/ic_inbox_active.svg
  SvgGenImage get icInboxActive =>
      const SvgGenImage('assets/icons/ic_inbox_active.svg');

  /// File path: assets/icons/ic_kakaotalk.svg
  SvgGenImage get icKakaotalk =>
      const SvgGenImage('assets/icons/ic_kakaotalk.svg');

  /// File path: assets/icons/ic_layout.svg
  SvgGenImage get icLayout => const SvgGenImage('assets/icons/ic_layout.svg');

  /// File path: assets/icons/ic_layout_active.svg
  SvgGenImage get icLayoutActive =>
      const SvgGenImage('assets/icons/ic_layout_active.svg');

  /// File path: assets/icons/ic_log_out.svg
  SvgGenImage get icLogOut => const SvgGenImage('assets/icons/ic_log_out.svg');

  /// File path: assets/icons/ic_login.svg
  SvgGenImage get icLogin => const SvgGenImage('assets/icons/ic_login.svg');

  /// File path: assets/icons/ic_logo_splash.svg
  SvgGenImage get icLogoSplash =>
      const SvgGenImage('assets/icons/ic_logo_splash.svg');

  /// File path: assets/icons/ic_naver.svg
  SvgGenImage get icNaver => const SvgGenImage('assets/icons/ic_naver.svg');

  /// File path: assets/icons/ic_new_feed_tab_active.svg
  SvgGenImage get icNewFeedTabActive =>
      const SvgGenImage('assets/icons/ic_new_feed_tab_active.svg');

  /// File path: assets/icons/ic_new_feed_tab_inactive.svg
  SvgGenImage get icNewFeedTabInactive =>
      const SvgGenImage('assets/icons/ic_new_feed_tab_inactive.svg');

  /// File path: assets/icons/ic_phone.svg
  SvgGenImage get icPhone => const SvgGenImage('assets/icons/ic_phone.svg');

  /// File path: assets/icons/ic_search.svg
  SvgGenImage get icSearch => const SvgGenImage('assets/icons/ic_search.svg');

  /// File path: assets/icons/ic_shopping_bag.svg
  SvgGenImage get icShoppingBag =>
      const SvgGenImage('assets/icons/ic_shopping_bag.svg');

  /// File path: assets/icons/ic_user.svg
  SvgGenImage get icUser => const SvgGenImage('assets/icons/ic_user.svg');

  /// File path: assets/icons/ic_user_active.svg
  SvgGenImage get icUserActive =>
      const SvgGenImage('assets/icons/ic_user_active.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        icAddress,
        icArrowDown,
        icArrowDowns,
        icArrowLeft,
        icBell,
        icBoxChat,
        icCalendar,
        icChat,
        icChatActive,
        icCheckCircle,
        icCheckCircleActive,
        icChevronDown,
        icClock,
        icClose,
        icCloses,
        icCommunity,
        icCommunityActive,
        icEyeOff,
        icHome,
        icHomeActive,
        icImage,
        icInbox,
        icInboxActive,
        icKakaotalk,
        icLayout,
        icLayoutActive,
        icLogOut,
        icLogin,
        icLogoSplash,
        icNaver,
        icNewFeedTabActive,
        icNewFeedTabInactive,
        icPhone,
        icSearch,
        icShoppingBag,
        icUser,
        icUserActive
      ];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/ic_empty.png
  AssetGenImage get icEmpty =>
      const AssetGenImage('assets/images/ic_empty.png');

  /// File path: assets/images/image_1.png
  AssetGenImage get image1 => const AssetGenImage('assets/images/image_1.png');

  /// File path: assets/images/image_2.png
  AssetGenImage get image2 => const AssetGenImage('assets/images/image_2.png');

  /// File path: assets/images/image_3.png
  AssetGenImage get image3 => const AssetGenImage('assets/images/image_3.png');

  /// File path: assets/images/image_4.png
  AssetGenImage get image4 => const AssetGenImage('assets/images/image_4.png');

  /// File path: assets/images/image_5.png
  AssetGenImage get image5 => const AssetGenImage('assets/images/image_5.png');

  /// File path: assets/images/register_success.png
  AssetGenImage get registerSuccess =>
      const AssetGenImage('assets/images/register_success.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [icEmpty, image1, image2, image3, image4, image5, registerSuccess];
}

class Assets {
  Assets._();

  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
