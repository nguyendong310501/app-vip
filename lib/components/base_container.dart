import 'package:flutter/material.dart';
import 'package:app_oke_vip/resources/resources.dart';
import 'package:app_oke_vip/utilities/helpers/appbar_helper/appbar_helper.dart';

class BaseContainer extends StatelessWidget {
  final Widget body;
  final Color? backgroundColor;
  final Color? backgroundColorAppBar;
  const BaseContainer({
    super.key,
    required this.body,
    this.backgroundColor,
    this.backgroundColorAppBar,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor ?? context.colors.backgroundSecondary,
      appBar: appBarCustom(backgroundColorAppBar),
      body: body,
    );
  }
}
