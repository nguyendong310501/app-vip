import 'package:app_oke_vip/resources/resources.dart';
import 'package:app_oke_vip/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../resources/app_text_styles.dart';

class EmptyDataWidget extends StatelessWidget {
  final double padding;
  const EmptyDataWidget({
    super.key,
    this.padding = 50,
  });

  @override
  Widget build(BuildContext context) => Align(
        alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: padding),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                AppImages.imgEmpty,
                width: 185.w,
                height: 211.h,
              ),
              const SizedBox(height: 4.0),
              Text(
                S.of(context)!.text_common_text_empty,
                style: AppTextStyles.labelRegular14,
              ),
            ],
          ),
        ),
      );
}
