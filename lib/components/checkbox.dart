// ignore_for_file: no_logic_in_create_state

import 'package:app_oke_vip/presentation/base/base_page.dart';
import 'package:flutter/material.dart';
import 'package:app_oke_vip/resources/resources.dart';


class CheckBoxCustom extends BasePage {
  final Function onChanged;
  final bool checked;
  final double? borderRadius;

  const CheckBoxCustom(
      {super.key,
      required this.onChanged,
      required this.checked,
      this.borderRadius});

  @override
  State<CheckBoxCustom> createState() => _CheckBoxCustomState();
}

class _CheckBoxCustomState extends State<CheckBoxCustom> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onChanged();
      },
      // splashColor: Colors.grey,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: widget.checked
                ? context.colors.backgroundButon
                : context.colors.border,
          ),
          color: widget.checked ? context.colors.h133145 : context.colors.hE3E5E5,
          borderRadius:
              BorderRadius.all(Radius.circular(widget.borderRadius ?? 100.0)),
        ),
        child: SizedBox(
          width: 20.0,
          height: 20.0,
          child:  Icon(
                  Icons.check,
                  color: context.colors.backgroundWhite,
                  size: 20,
                )

        ),
      ),
    );
  }
}
