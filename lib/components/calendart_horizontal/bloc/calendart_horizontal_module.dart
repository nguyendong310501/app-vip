import 'package:app_oke_vip/injection/injector.dart';

import 'calendart_horizontal_persenter.dart';

class CalendartHorizontalModule {
  static Future<void> inject() async {
    injector.registerLazySingleton<CalendartHorizontalPersenter>(
      () => CalendartHorizontalPersenter(),
    );
  }
}
