import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:app_oke_vip/resources/app_colors.dart';
import 'package:app_oke_vip/resources/app_text_styles.dart';
import 'package:app_oke_vip/utilities/extensions/date_extensions.dart';
import 'package:app_oke_vip/utilities/helpers/timer_helper/timer_helper.dart';

class CustomCalendarItem {
  final String content;
  final DateTime date;

  const CustomCalendarItem({required this.date, required this.content});
}

class DPCalendar extends StatefulWidget {
  const DPCalendar({
    super.key,
    required this.selectedDay,
    this.onDaySelected,
    this.enabledDayPredicate,
    required this.focusedDay,
  });

  final DateTime selectedDay;
  final DateTime focusedDay;
  final void Function(DateTime, DateTime)? onDaySelected;
  final bool Function(DateTime)? enabledDayPredicate;

  @override
  State<DPCalendar> createState() => _DPCalendarState();
}

class _DPCalendarState extends State<DPCalendar> {
  @override
  Widget build(BuildContext context) {
    return TableCalendar(
      availableGestures: AvailableGestures.none,
      firstDay: DateTime.utc(2023, 1, 1),
      lastDay: DateTime.utc(2030, 1, 1),
      focusedDay: widget.focusedDay,
      selectedDayPredicate: (DateTime day) =>
          widget.selectedDay.isSameDate(day),
      calendarFormat: CalendarFormat.month,
      locale: 'ko_KO',
      startingDayOfWeek: StartingDayOfWeek.monday,
      daysOfWeekHeight: 43,
      rowHeight: 43,
      daysOfWeekStyle: const DaysOfWeekStyle(
        decoration: BoxDecoration(),
        weekdayStyle: TextStyle(color: Colors.grey),
        weekendStyle: TextStyle(color: Colors.grey),
      ),
      headerStyle: HeaderStyle(
        formatButtonVisible: false,
        titleCentered: false,
        titleTextStyle:
            AppTextStyles.labelMedium16.copyWith(color: context.colors.title),
        leftChevronVisible: false,
        rightChevronVisible: false,
      ),
      headerVisible: false,
      calendarStyle: CalendarStyle(
        // tableBorder: TableBorder.all(color: context.colors.backgroundButon),
        tablePadding: const EdgeInsets.only(bottom: 24),
        todayTextStyle: TextStyle(color: context.colors.backgroundWhite),
        selectedDecoration: BoxDecoration(
          color: context.colors.backgroundSecondary,
        ),
        todayDecoration: BoxDecoration(
          color: context.colors.backgroundSecondary,
        ),
      ),
      calendarBuilders: CalendarBuilders(
        selectedBuilder: (BuildContext context, DateTime day, _) =>
            makeDayCell(context, day, true, false),
        todayBuilder: (BuildContext context, DateTime day, _) =>
            makeDayCell(context, day, false, false),
        defaultBuilder: (BuildContext context, DateTime day, _) =>
            makeDayCell(context, day, false, false),
        disabledBuilder: (BuildContext context, DateTime day, _) =>
            makeDayCell(context, day, false, true),
      ),
      onDaySelected: widget.onDaySelected,
      enabledDayPredicate: widget.enabledDayPredicate,
    );
  }

  Widget makeDayCell(
      BuildContext context, DateTime day, bool isFocus, bool isDisabled) {
    return DayCell(
      date: day,
      note: '',
      isFocus: isFocus,
      isDisabled: isDisabled,
    );
  }
}

class DayCell extends StatelessWidget {
  const DayCell({
    super.key,
    required this.date,
    required this.note,
    required this.isFocus,
    required this.isDisabled,
  });

  final DateTime date;
  final String note;
  final bool isFocus;
  final bool isDisabled;

  @override
  Widget build(BuildContext context) {
    final bool isToday = date.ymd == DateTime.now().ymd;
    return Container(
      width: 40.w,
      height: 40.h,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: context.colors.backgroundWhite,
          border: Border.all(
              color: isFocus ? context.colors.backgroundButon : Colors.white,
              width: 1.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: note.isNotEmpty ? 1 : 0,
            child: Container(
              alignment: Alignment.center,
              child: Text(
                DateFormat.d().format(date),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: isDisabled
                      ? Colors.grey
                      : isFocus
                          ? isToday
                              ? context.colors.title
                              : context.colors.title
                          : context.colors.title,
                  fontWeight: FontWeight.w400,
                  fontSize: 13,
                ),
              ),
            ),
          ),
          note.isNotEmpty
              ? Expanded(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.center,
                    color: note.isNotEmpty ? context.colors.title : null,
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: Text(
                      note,
                      style: TextStyle(
                        color: note.isNotEmpty
                            ? context.colors.title
                            : isDisabled
                                ? Colors.grey
                                : isFocus
                                    ? Colors.white
                                    : context.colors.title,
                        fontWeight: FontWeight.w400,
                        fontSize: 10,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
