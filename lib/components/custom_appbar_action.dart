import 'package:app_oke_vip/resources/app_colors.dart';
import 'package:app_oke_vip/components/svg_icon.dart';
import 'package:app_oke_vip/resources/resources.dart';
import 'package:flutter/material.dart';


class CustomAppBarAction extends StatefulWidget {
  const CustomAppBarAction({
    super.key,
    this.onTap,
    this.iconPath,
    this.haveBadge = false,
    this.badgeContent,
    this.padding,
    this.backgroundColor,
  });

  final Function? onTap;
  final String? iconPath;
  final bool haveBadge;
  final String? badgeContent;
  final EdgeInsetsGeometry? padding;
  final Color? backgroundColor;

  @override
  State<CustomAppBarAction> createState() => _CustomAppBarActionState();
}

class _CustomAppBarActionState extends State<CustomAppBarAction> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      child: GestureDetector(
        onTap: () {
          widget.onTap?.call();
        },
        child: Stack(
          children: <Widget>[
            Container(
              width: 36.0,
              height: 36.0,
              padding: const EdgeInsets.all(6.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(200),
                color: widget.backgroundColor ??
                    context.colors.backgroundSecondary,
              ),
              child: SvgIcon.from(widget.iconPath ?? AppIcons.icSearch,
                  color: context.colors.backgroundPrimary),
            ),
            widget.haveBadge
                ? Positioned(
                    right: 1.0,
                    top: 1.0,
                    child: Container(
                      width: 14.0,
                      height: 14.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(200),
                        color: context.colors.textSecondary,
                      ),
                      child: Text(
                        widget.badgeContent ?? '',
                        style: AppTextStyles.labelRegular10.copyWith(
                          color: context.colors.labelSecondary,
                        ),
                      ),
                    ))
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
