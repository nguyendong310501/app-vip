import 'dart:async';

import 'package:app_oke_vip/resources/app_colors.dart';
import 'package:app_oke_vip/generated/l10n.dart';
import 'package:app_oke_vip/components/custom_search_bar.dart';
import 'package:flutter/material.dart';

class CustomSearchBarDebounce extends StatefulWidget {
  const CustomSearchBarDebounce({
    super.key,
    required this.value,
    required this.onSearch,
    this.hintText,
    this.haveIcon,
    this.onSummited,
  });

  final String value;
  final Function(String) onSearch;
  final String? hintText;
  final bool? haveIcon;
  final Function(String)? onSummited;

  @override
  State<CustomSearchBarDebounce> createState() =>
      _CustomSearchBarDebounceState();
}

class _CustomSearchBarDebounceState extends State<CustomSearchBarDebounce> {
  Timer? _debounceTimer;
  String searchText = '';

  void handleSearch() {
    if (_debounceTimer?.isActive ?? false) _debounceTimer?.cancel();
    _debounceTimer = Timer(const Duration(milliseconds: 700), () async {
      widget.onSearch(searchText);
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      searchText = widget.value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      color: context.colors.backgroundWhite,
      child: CustomSearchBar(
        hintText:
            widget.hintText ?? S.of(context)!.text_common_search,
        haveIcon: widget.haveIcon ?? true,
        value: searchText,
        onChange: (String text) {
          setState(() {
            searchText = text;
          });
          handleSearch();
        },
        onSummited: (String text) {
          widget.onSummited?.call(text);
        },
      ),
    );
  }
}
