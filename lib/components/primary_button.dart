import 'package:dartx/dartx.dart';
import 'package:flutter/cupertino.dart';
import 'package:app_oke_vip/presentation/base/base_page.dart';
import 'package:app_oke_vip/resources/app_colors.dart';
import 'package:app_oke_vip/resources/app_text_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class PrimaryButton extends BasePage {
  final String title;
  final bool isDisable;
  final bool isLoading;
  final Color? backgroundColor;
  final Color? textColor;
  final Color? borderColor;
  final Color? colorLoading;
  VoidCallback? onPressed;
  EdgeInsets? padding;
  double? height;
  bool removeHeight;
  String? icon;
  bool isBoderLeft;
  bool isBoderRight;
  TextStyle? textStyle;
  bool isCenterTop;

  PrimaryButton(
      {super.key,
      required this.title,
      this.isCenterTop = false,
      this.isDisable = false,
      this.isLoading = false,
      this.isBoderLeft = false,
      this.isBoderRight = false,
      this.backgroundColor,
      this.textColor,
      this.borderColor,
      this.onPressed,
      this.padding,
      this.height = 44.0,
      this.colorLoading,
      this.removeHeight = false,
      this.icon,
      this.textStyle});

  @override
  State<PrimaryButton> createState() => _PrimaryButtonState();
}

class _PrimaryButtonState extends State<PrimaryButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (widget.isDisable || widget.isLoading) ? null : widget.onPressed,
      child: Container(
          alignment: widget.isCenterTop ? Alignment.topCenter : null,
          height: !widget.removeHeight ? widget.height : null,
          padding: widget.padding,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              color: widget.isDisable
                  ? context.colors.border
                  : widget.borderColor ?? context.colors.h011E32,
            ),
            color: widget.isDisable
                ? context.colors.border
                : widget.backgroundColor ?? context.colors.h011E32,
            borderRadius: BorderRadius.only(
                bottomLeft: widget.isBoderLeft
                    ? const Radius.circular(10.0)
                    : const Radius.circular(4.0),
                bottomRight: widget.isBoderRight
                    ? const Radius.circular(10.0)
                    : const Radius.circular(4.0),
                topLeft: const Radius.circular(4.0),
                topRight: const Radius.circular(4.0)),
          ),
          child: widget.isLoading
              ? Center(
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 16.0,
                          width: 16.0,
                          child: CupertinoActivityIndicator(
                            color: widget.colorLoading ??
                                context.colors.backgroundSecondary,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (widget.icon.isNotNullOrBlank)
                      Padding(
                        padding: const EdgeInsets.only(right: 6.0),
                        child: SvgPicture.asset(widget.icon!),
                      ),
                    Text(
                      widget.title.toString(),
                      style: widget.textStyle ??
                          AppTextStyles.labelBold16.copyWith(
                            color: widget.textColor ??
                                context.colors.backgroundWhite,
                          ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
    );
  }
}
