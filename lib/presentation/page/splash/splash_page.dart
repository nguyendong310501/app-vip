import 'package:app_oke_vip/presentation/page/main/main_page.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:app_oke_vip/resources/resources.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/page/authen/login/login_page.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Future<dynamic>.delayed(const Duration(milliseconds: 1500))
        .then((value) => {
              if (injector.get<AuthPreferencesRepository>().getAccessToken() ==
                  null)
                {
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => const LoginPage(),
                    ),
                    ModalRoute.withName('/'),
                  )
                }
              else
                {
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => const MainPage(),
                    ),
                    ModalRoute.withName('/'),
                  )
                }
            });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = ((MediaQuery.of(context).size.height) -
            MediaQuery.of(context).padding.top) /
        812.0;
    double width = (MediaQuery.of(context).size.width) / 375.0;
    return Scaffold(
      backgroundColor: context.colors.backgroundSecondary,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: context.colors.h011E32,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'App oke vip',
                style: AppTextStyles.labelBold36
                    .copyWith(color: context.colors.backgroundWhite),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
