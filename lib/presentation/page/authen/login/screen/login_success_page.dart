import 'package:app_oke_vip/resources/app_colors.dart';
import 'package:app_oke_vip/resources/app_text_styles.dart';
import 'package:app_oke_vip/components/base_container.dart';
import 'package:flutter/material.dart';

class LoginSuccessPage extends StatefulWidget {
  final String externalUserId;
  final String userName;
  final Function function;
  const LoginSuccessPage(
    this.externalUserId,
    this.userName,
    this.function, {
    super.key,
  });

  @override
  State<LoginSuccessPage> createState() => _LoginSuccessPageState();
}

class _LoginSuccessPageState extends State<LoginSuccessPage> {
  @override
  Widget build(BuildContext context) {
    return BaseContainer(
        body: Center(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Hello : ${widget.userName}',
          style:
              AppTextStyles.labelBold16.copyWith(color: context.colors.title),
        ),
        const SizedBox(
          height: 24.0,
        ),
        Text(
          'User Id : ${widget.externalUserId}',
          style:
              AppTextStyles.labelBold16.copyWith(color: context.colors.title),
        ),
        const SizedBox(
          height: 24.0,
        ),
        InkResponse(
          onTap: () => widget.function.call(),
          child: Text(
            'Logout now',
            style:
                AppTextStyles.labelBold10.copyWith(color: context.colors.title),
          ),
        ),
      ],
    )));
  }
}
