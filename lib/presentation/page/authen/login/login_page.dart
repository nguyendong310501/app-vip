// ignore_for_file: library_private_types_in_public_api

/* L-01 Log in */

import 'package:app_oke_vip/resources/resources.dart';
import 'package:app_oke_vip/presentation/page/authen/login/bloc/login_presenter.dart';
import 'package:app_oke_vip/components/custom_appbar.dart';
import 'package:app_oke_vip/components/primary_button.dart';
import 'package:app_oke_vip/components/text_input_custom.dart';
import 'package:app_oke_vip/utilities/helpers/form_field/email.dart';
import 'package:app_oke_vip/utilities/helpers/form_field/password.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_oke_vip/presentation/base/base_page.dart';
import 'package:app_oke_vip/presentation/base/progress_hud_mixin.dart';
import 'package:app_oke_vip/utilities/helpers/appbar_helper/appbar_helper.dart';

import 'bloc/login_state.dart';

class LoginPage extends BasePage {
  const LoginPage({
    super.key,
  });

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BasePageState<LoginPage>
    with ProgressHudMixin<LoginPage> {
  final _loginPresenter = LoginPresenter();

  @override
  void initState() {
    _loginPresenter.init();
    super.initState();
  }

  @override
  void dispose() {
    _loginPresenter.cleanState();
    super.dispose();
  }

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return CustomAppBar(
      backgroundColorAppBar: context.colors.backgroundWhite,
      label: 'Login Page',
      isBorderBottom: true,
      isBack: false,
      isCenterTitle: true,
      labelStyle: AppTextStyles.labelBold18.copyWith(
        color: context.colors.title,
      ),
      brightness: Brightness.dark,
    );
  }

  @override
  Widget buildBody(BuildContext context) =>
      BlocConsumer<LoginPresenter, LoginState>(
        bloc: _loginPresenter,
        listenWhen: (LoginState previous, LoginState current) =>
            (previous != current),
        listener: (BuildContext context, LoginState state) {},
        builder: (BuildContext context, LoginState state) {
          double height = ((MediaQuery.of(context).size.height) -
                  MediaQuery.of(context).padding.top -
                  63.0) /
              812.0;
          double width = (MediaQuery.of(context).size.width) / 375.0;

          return GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Scaffold(
              backgroundColor: context.colors.backgroundWhite,
              appBar: appBarCustom(context.colors.backgroundWhite),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 21.0 * width,
                  ),
                  color: context.colors.backgroundWhite,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 47.0 * height),
                      RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            TextSpan(
                              text: 'App ',
                              style: AppTextStyles.labelMedium26
                                  .copyWith(color: context.colors.title),
                            ),
                            TextSpan(
                              text: 'oke vip',
                              style: AppTextStyles.labelBold26
                                  .copyWith(color: context.colors.title),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 37.0 * height),
                      TextInputCustom(
                        hintText: 'Please enter user name',
                        labelText: 'User name',
                        onChanged: (String text) =>
                            _loginPresenter.emailChanged(text),
                        errorMessage: state.email.error?.description,
                        keyboardType: TextInputType.emailAddress,
                        cursorColor: context.colors.backgroundPrimary,
                        isRequired: false,
                      ),
                      SizedBox(height: 18.0 * height),
                      TextInputCustom(
                        hintText: 'Please enter password',
                        labelText: 'Password',
                        onChanged: (String text) =>
                            _loginPresenter.passwordChanged(text),
                        errorMessage: state.password.error?.description,
                        isPasswordField: true,
                        cursorColor: context.colors.backgroundPrimary,
                      ),
                      SizedBox(height: 18.0 * height),
                      PrimaryButton(
                        title: 'Login',
                        isLoading: state.isLoadingLogin,
                        isDisable: _loginPresenter.isDisable,
                        onPressed: () => _loginPresenter.handleLogin(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
}
