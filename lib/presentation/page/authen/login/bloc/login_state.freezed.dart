// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'login_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoginState {
  Email get email => throw _privateConstructorUsedError;
  PasswordSignUp get password => throw _privateConstructorUsedError;
  FormzStatus get status => throw _privateConstructorUsedError;
  Object? get error => throw _privateConstructorUsedError;
  bool get isStatusPassword => throw _privateConstructorUsedError;
  bool get isLoadingLogin => throw _privateConstructorUsedError;
  String? get initEmail => throw _privateConstructorUsedError;
  bool get isDisableKakao => throw _privateConstructorUsedError;
  bool get isDisableNaver => throw _privateConstructorUsedError;
  bool get keepLogin => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginStateCopyWith<LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res, LoginState>;
  @useResult
  $Res call(
      {Email email,
      PasswordSignUp password,
      FormzStatus status,
      Object? error,
      bool isStatusPassword,
      bool isLoadingLogin,
      String? initEmail,
      bool isDisableKakao,
      bool isDisableNaver,
      bool keepLogin});
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res, $Val extends LoginState>
    implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
    Object? password = null,
    Object? status = null,
    Object? error = freezed,
    Object? isStatusPassword = null,
    Object? isLoadingLogin = null,
    Object? initEmail = freezed,
    Object? isDisableKakao = null,
    Object? isDisableNaver = null,
    Object? keepLogin = null,
  }) {
    return _then(_value.copyWith(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as Email,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as PasswordSignUp,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      error: freezed == error ? _value.error : error,
      isStatusPassword: null == isStatusPassword
          ? _value.isStatusPassword
          : isStatusPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingLogin: null == isLoadingLogin
          ? _value.isLoadingLogin
          : isLoadingLogin // ignore: cast_nullable_to_non_nullable
              as bool,
      initEmail: freezed == initEmail
          ? _value.initEmail
          : initEmail // ignore: cast_nullable_to_non_nullable
              as String?,
      isDisableKakao: null == isDisableKakao
          ? _value.isDisableKakao
          : isDisableKakao // ignore: cast_nullable_to_non_nullable
              as bool,
      isDisableNaver: null == isDisableNaver
          ? _value.isDisableNaver
          : isDisableNaver // ignore: cast_nullable_to_non_nullable
              as bool,
      keepLogin: null == keepLogin
          ? _value.keepLogin
          : keepLogin // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LoginStateCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$_LoginStateCopyWith(
          _$_LoginState value, $Res Function(_$_LoginState) then) =
      __$$_LoginStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Email email,
      PasswordSignUp password,
      FormzStatus status,
      Object? error,
      bool isStatusPassword,
      bool isLoadingLogin,
      String? initEmail,
      bool isDisableKakao,
      bool isDisableNaver,
      bool keepLogin});
}

/// @nodoc
class __$$_LoginStateCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$_LoginState>
    implements _$$_LoginStateCopyWith<$Res> {
  __$$_LoginStateCopyWithImpl(
      _$_LoginState _value, $Res Function(_$_LoginState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
    Object? password = null,
    Object? status = null,
    Object? error = freezed,
    Object? isStatusPassword = null,
    Object? isLoadingLogin = null,
    Object? initEmail = freezed,
    Object? isDisableKakao = null,
    Object? isDisableNaver = null,
    Object? keepLogin = null,
  }) {
    return _then(_$_LoginState(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as Email,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as PasswordSignUp,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      error: freezed == error ? _value.error : error,
      isStatusPassword: null == isStatusPassword
          ? _value.isStatusPassword
          : isStatusPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingLogin: null == isLoadingLogin
          ? _value.isLoadingLogin
          : isLoadingLogin // ignore: cast_nullable_to_non_nullable
              as bool,
      initEmail: freezed == initEmail
          ? _value.initEmail
          : initEmail // ignore: cast_nullable_to_non_nullable
              as String?,
      isDisableKakao: null == isDisableKakao
          ? _value.isDisableKakao
          : isDisableKakao // ignore: cast_nullable_to_non_nullable
              as bool,
      isDisableNaver: null == isDisableNaver
          ? _value.isDisableNaver
          : isDisableNaver // ignore: cast_nullable_to_non_nullable
              as bool,
      keepLogin: null == keepLogin
          ? _value.keepLogin
          : keepLogin // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_LoginState extends _LoginState {
  _$_LoginState(
      {required this.email,
      required this.password,
      required this.status,
      this.error,
      required this.isStatusPassword,
      required this.isLoadingLogin,
      required this.initEmail,
      required this.isDisableKakao,
      required this.isDisableNaver,
      required this.keepLogin})
      : super._();

  @override
  final Email email;
  @override
  final PasswordSignUp password;
  @override
  final FormzStatus status;
  @override
  final Object? error;
  @override
  final bool isStatusPassword;
  @override
  final bool isLoadingLogin;
  @override
  final String? initEmail;
  @override
  final bool isDisableKakao;
  @override
  final bool isDisableNaver;
  @override
  final bool keepLogin;

  @override
  String toString() {
    return 'LoginState(email: $email, password: $password, status: $status, error: $error, isStatusPassword: $isStatusPassword, isLoadingLogin: $isLoadingLogin, initEmail: $initEmail, isDisableKakao: $isDisableKakao, isDisableNaver: $isDisableNaver, keepLogin: $keepLogin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginState &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other.error, error) &&
            (identical(other.isStatusPassword, isStatusPassword) ||
                other.isStatusPassword == isStatusPassword) &&
            (identical(other.isLoadingLogin, isLoadingLogin) ||
                other.isLoadingLogin == isLoadingLogin) &&
            (identical(other.initEmail, initEmail) ||
                other.initEmail == initEmail) &&
            (identical(other.isDisableKakao, isDisableKakao) ||
                other.isDisableKakao == isDisableKakao) &&
            (identical(other.isDisableNaver, isDisableNaver) ||
                other.isDisableNaver == isDisableNaver) &&
            (identical(other.keepLogin, keepLogin) ||
                other.keepLogin == keepLogin));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      email,
      password,
      status,
      const DeepCollectionEquality().hash(error),
      isStatusPassword,
      isLoadingLogin,
      initEmail,
      isDisableKakao,
      isDisableNaver,
      keepLogin);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      __$$_LoginStateCopyWithImpl<_$_LoginState>(this, _$identity);
}

abstract class _LoginState extends LoginState {
  factory _LoginState(
      {required final Email email,
      required final PasswordSignUp password,
      required final FormzStatus status,
      final Object? error,
      required final bool isStatusPassword,
      required final bool isLoadingLogin,
      required final String? initEmail,
      required final bool isDisableKakao,
      required final bool isDisableNaver,
      required final bool keepLogin}) = _$_LoginState;
  _LoginState._() : super._();

  @override
  Email get email;
  @override
  PasswordSignUp get password;
  @override
  FormzStatus get status;
  @override
  Object? get error;
  @override
  bool get isStatusPassword;
  @override
  bool get isLoadingLogin;
  @override
  String? get initEmail;
  @override
  bool get isDisableKakao;
  @override
  bool get isDisableNaver;
  @override
  bool get keepLogin;
  @override
  @JsonKey(ignore: true)
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}
