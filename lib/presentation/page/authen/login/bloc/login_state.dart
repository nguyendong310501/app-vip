
import 'package:app_oke_vip/utilities/helpers/form_field/email.dart';
import 'package:app_oke_vip/utilities/helpers/form_field/password.dart';
import 'package:formz/formz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

@freezed
class LoginState with _$LoginState {
  factory LoginState(
      {required Email email,
      required PasswordSignUp password,
      required FormzStatus status,
      Object? error,
      required bool isStatusPassword,
      required bool isLoadingLogin,
      required String? initEmail,
      required bool isDisableKakao,
      required bool isDisableNaver,
      required bool keepLogin}) = _LoginState;

  const LoginState._();

  factory LoginState.initial() => LoginState(
      email: Email.pure(),
      password: PasswordSignUp.pure(),
      status: FormzStatus.pure,
      error: null,
      isStatusPassword: false,
      isLoadingLogin: false,
      initEmail: "",
      isDisableKakao: false,
      isDisableNaver: false,
      keepLogin: false);
}
