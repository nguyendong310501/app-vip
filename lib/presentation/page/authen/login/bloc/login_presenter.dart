import 'package:app_oke_vip/presentation/page/authen/login/bloc/login_state.dart';
import 'package:app_oke_vip/presentation/page/authen/login/login_page.dart';
import 'package:app_oke_vip/presentation/page/authen/login/screen/login_success_page.dart';
import 'package:app_oke_vip/presentation/page/main/bloc/main_page_state.dart';
import 'package:app_oke_vip/presentation/page/main/main_page.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:app_oke_vip/remote/network_services/api_client/api_client.dart';
import 'package:app_oke_vip/remote/network_services/body/user/info/response/user_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login/login_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login/login_output_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login_kakao/login_kakao_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login_naver/login_naver_input_model.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';
import 'package:app_oke_vip/utilities/helpers/form_field/email.dart';
import 'package:app_oke_vip/utilities/helpers/form_field/password.dart';
import 'package:dio/dio.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/utilities/helpers/language_helper/language_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:formz/formz.dart';
import 'package:kakao_flutter_sdk/kakao_flutter_sdk.dart';

class LoginPresenter extends Cubit<LoginState> {
  LoginPresenter({
    @visibleForTesting LoginState? state,
  }) : super(state ?? LoginState.initial());

  final _apiClient = injector.get<ApiClient>();
  final _auth = injector.get<AuthPreferencesRepository>();

  void init() {}

  void resetFormSignIn() {
    emit(state.copyWith(
      password: PasswordSignUp.pure(),
      email: Email.pure(),
      status: FormzStatus.pure,
    ));
  }

  void setInfoLogin() {}

  void cleanState() {
    emit(LoginState.initial());
  }

  void handleVisibilityPassword() {
    emit(state.copyWith(isStatusPassword: !state.isStatusPassword));
  }

  void handleKeepLogin() {
    emit(state.copyWith(keepLogin: !state.keepLogin));
  }

  void emailChanged(String value) {
    final Email email = Email.dirty(value);
    emit(state.copyWith(
      email: email,
      status: Formz.validate(<FormzInput<dynamic, dynamic>>[
        email,
        state.password,
      ]),
    ));
  }

  void passwordChanged(String value) {
    final PasswordSignUp password = PasswordSignUp.dirty(value);
    emit(state.copyWith(
      password: password,
      status: Formz.validate(<FormzInput<dynamic, dynamic>>[
        state.email,
        password,
      ]),
    ));
  }

  void clearState() {
    emit(LoginState.initial());
  }

  bool get isDisable => !state.status.isValidated;

  Future<void> handleLogin() async {
    emit(state.copyWith(isLoadingLogin: true));
    try {
      final LoginResponseModel response =
          await _apiClient.login(LoginInputModel(
        password: state.password.value ?? '',
        email: state.email.value ?? '',
      ));

      await _auth.setAccessToken(response.data!.token);

      await uploadDeviceToken();
      handleLoginSuccess();
    } catch (error) {
      await _auth.clearAll();
      handleError(error);
    } finally {
      emit(state.copyWith(isLoadingLogin: false));
    }

    emit(state.copyWith(isLoadingLogin: false));
  }

  Future<void> uploadDeviceToken() async {
    try {} catch (error) {}
  }

  handleLoginSuccess() async {
    final UserResponseModel responseUser = await _apiClient.getInfoUsers();

    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => MainPage(),
      ),
      ModalRoute.withName('/'),
    );
  }

  Future<void> handleLogout() async {
    try {
      await _auth.clearAll();
      injector.get<ScaffoldGlobalContextHelper>().resetKey();
      handleLogoutSuccess();
    } catch (error) {
      handleError(error);
    }
  }

  void handleLogoutSuccess() {
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => const LoginPage(),
      ),
      ModalRoute.withName('/'),
    );
  }

  void handleError(Object? error) {
    emit(state.copyWith(isLoadingLogin: false));

    String errorOther = LanguageHelper.of(context).getLocalText("다른 에러");

    if (error is DioError) {
      dynamic messages =
          error.response?.data?['error_message']['message'] ?? <dynamic>[];

      if (messages.isNotEmpty) {
        return showPopup(
          error: messages,
        );
      } else {
        showPopup(error: errorOther);
      }
    } else {
      showPopup(error: errorOther);
    }
  }

  String getEmail() {
    return state.email.value ?? "";
  }

  Future<void> handelLoginWithKakao(OAuthToken token) async {
    try {
      final LoginResponseModel response =
          await _apiClient.loginWithKakao(LoginKakaoInputModel(
        idToken: token.idToken!,
        accessToken: token.accessToken,
      ));
      await _auth.setAccessToken(response.data!.token);

      uploadDeviceToken();
      handleLoginSuccess();
    } catch (error) {
      await _auth.clearAll();

      handleError(error);
    }
  }

  Future<void> loginWithNaverSuccess(NaverAccessToken resdata) async {
    try {
      final LoginResponseModel response =
          await _apiClient.loginWithNaver(LoginNaverInputModel(
        accessToken: resdata.accessToken,
      ));

      await _auth.setAccessToken(response.data!.token);

      uploadDeviceToken();
      handleLoginSuccess();
    } catch (error) {
      await _auth.clearAll();

      handleError(error);
    }
  }
}
