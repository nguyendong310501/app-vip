import 'dart:async';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/page/main/bloc/main_page_state.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:math_expressions/math_expressions.dart';
import '../../authen/login/login_page.dart';

class MainPagePresenter extends Cubit<MainPageState> {
  MainPagePresenter(
    this._scaffoldGlobalContextHelper, {
    @visibleForTesting MainPageState? state,
  }) : super(MainPageState.initial());
  final ScaffoldGlobalContextHelper _scaffoldGlobalContextHelper;

  ScrollController controller = ScrollController();
  final _auth = injector.get<AuthPreferencesRepository>();

  /// Button Pressed
  equalPressed() {
    String userInputFC = state.userInput ?? '';
    userInputFC = userInputFC.replaceAll("x", "*");
    Parser p = Parser();
    Expression exp = p.parse(userInputFC);
    ContextModel ctx = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, ctx);

    emit(state.copyWith(userOutput: eval.toString()));
  }

  /// Delete Button
  deleteBtnAction() {
    emit(state.copyWith(
        userInput: state.userInput!.substring(0, state.userInput!.length - 1)));
  }

  /// Button Tapped
  onBtnTapped(List<String> buttons, int index) {
    if (state.userInput!.characters.length > 1 &&
        '-+x/'.contains(buttons[index])) {
      if ('-+x'.contains(state.userInput!.characters.last)) {
        return;
      }
    }
    var result = state!.userInput! + buttons[index];
    emit(state.copyWith(userInput: result));
    scrollAuto();
  }

  void cleanState() {
    emit(MainPageState.initial());
  }

  void scrollAuto() {
    controller.animateTo(controller.position.maxScrollExtent,
        duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
  }

  Future<void> handleLogout() async {
    try {
      await _auth.clearAll();
      injector.get<ScaffoldGlobalContextHelper>().resetKey();
      handleLogoutSuccess();
    } catch (error) {
      handleError(error);
    }
  }

  void handleLogoutSuccess() {
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => const LoginPage(),
      ),
      ModalRoute.withName('/'),
    );
  }

  void handleError(Object? error) {
    if (error is DioError) {
      dynamic messages =
          error.response?.data?['error_message']['message'] ?? <dynamic>[];

      return showPopup(
        error: messages,
      );
    }
  }
}
