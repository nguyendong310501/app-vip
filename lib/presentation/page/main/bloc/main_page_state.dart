import 'package:app_oke_vip/gen/assets.gen.dart';
import 'package:app_oke_vip/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:app_oke_vip/resources/app_icons.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/components/custom_popup.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/navigator_global_context_helper.dart';
import 'package:geolocator/geolocator.dart';

part 'main_page_state.freezed.dart';

enum StatusWorkingEmployee { Online, Offline, Washing }

final BuildContext context =
    injector.get<NavigatorGlobalContextHelper>().getCurrentContext;

void showPopup({
  String error = "",
  bool isCancle = false,
  VoidCallback? onHandleConfirm,
  String title = "Đóng",
}) {
  Navigator.push(
    context,
    CustomPopup(
      message: error,
      isCancle: isCancle,
      onHandleConfirm: onHandleConfirm,
      title: title,
    ),
  );
}

void navigationEventsHelper(Widget builder) {
  Navigator.of(context).push(
    MaterialPageRoute<dynamic>(
      builder: (BuildContext context) => builder,
    ),
  );
}

enum MainPageBottom {
  home,
  inbox,
  community,
  chat,
  account;

  String get bottomTitle {
    switch (this) {
      case MainPageBottom.home:
        return '홈';
      case MainPageBottom.inbox:
        return '요청 리스트';
      case MainPageBottom.community:
        return '커뮤니티';
      case MainPageBottom.chat:
        return '채팅';
      case MainPageBottom.account:
        return '프로필';
    }
  }

  String get activeIcon {
    switch (this) {
      case MainPageBottom.home:
        return Assets.icons.icHomeActive.path;
      case MainPageBottom.inbox:
        return Assets.icons.icInboxActive.path;
      case MainPageBottom.community:
        return Assets.icons.icCommunityActive.path;
      case MainPageBottom.chat:
        return Assets.icons.icChatActive.path;
      case MainPageBottom.account:
        return Assets.icons.icLayoutActive.path;
    }
  }

  String get normalIcon {
    switch (this) {
      case MainPageBottom.home:
        return Assets.icons.icHome.path;
      case MainPageBottom.inbox:
        return Assets.icons.icInbox.path;
      case MainPageBottom.community:
        return Assets.icons.icCommunity.path;
      case MainPageBottom.chat:
        return Assets.icons.icChat.path;
      case MainPageBottom.account:
        return Assets.icons.icLayout.path;
    }
  }
}

@freezed
class MainPageState with _$MainPageState {
  factory MainPageState(
      {required String? userInput,
      required String? userOutput,
     }) = _MainPageState;

  const MainPageState._();

  factory MainPageState.initial() => MainPageState(
      userInput: '',
      userOutput: '',
   );
}
