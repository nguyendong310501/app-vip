
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/page/main/bloc/main_page_presenter.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';
import 'package:app_oke_vip/utilities/helpers/fcm_push_notification/fcm_push_notification.dart';

class MainPageModule {
  static Future<void> inject() async {
    injector.registerLazySingleton<MainPagePresenter>(
      () => MainPagePresenter(
          injector.get<ScaffoldGlobalContextHelper>(),
    ));
  }
}
