// ignore_for_file: library_private_types_in_public_api, always_specify_types

import 'package:app_oke_vip/resources/resources.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/app_router.dart';
import 'package:app_oke_vip/presentation/base/base_page.dart';
import 'package:app_oke_vip/presentation/base/progress_hud_mixin.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/main_page_presenter.dart';
import 'bloc/main_page_state.dart';

class MainPage extends BasePage {
  const MainPage({
    super.key,
  });

  static ScreenRoute get route => ScreenRoute(
        name: '/home',
        builder: (_) => const MainPage(),
      );

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends BasePageState<MainPage>
    with ProgressHudMixin, WidgetsBindingObserver {
  final List<String> buttons = [
    "C",
    "DEL",
    "%",
    "/",
    "9",
    "8",
    "7",
    "x",
    "6",
    "5",
    "4",
    "-",
    "3",
    "2",
    "1",
    "+",
    "0",
    ".",
    "ANS",
    "=",
  ];

  final MainPagePresenter _mainPagePresenter =
      injector.get<MainPagePresenter>();
  final GlobalKey<NavigatorState> homeNavigatorKey =
      GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _mainPagePresenter.cleanState();
    super.dispose();
  }

  @override
  Widget buildBody(BuildContext context) {
    return BlocConsumer<MainPagePresenter, MainPageState>(
      bloc: _mainPagePresenter,
      listener: (BuildContext context, MainPageState state) {},
      builder: (BuildContext context, MainPageState state) => Scaffold(
        backgroundColor: const Color(0xff292D36),
        body: Column(
          children: [
            Expanded(
                child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    /// Main Result - user input and output
                    Expanded(
                      child: SingleChildScrollView(
                        controller: _mainPagePresenter.controller,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20, top: 50),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  state.userInput ?? '',
                                  style: AppTextStyles.labelBold36.copyWith(
                                      color: context.colors.backgroundWhite),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        state.userOutput ?? "",
                        style: AppTextStyles.labelBold36.copyWith(
                            color: context.colors.backgroundWhite,
                            fontSize: 36),
                      ),
                    ),
                  ],
                ),
                Positioned(
                    top: 25,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: GestureDetector(
                        onTap: () => _mainPagePresenter.handleLogout(),
                        child: Text(
                          'Logout',
                          style: AppTextStyles.labelBold16
                              .copyWith(color: context.colors.backgroundWhite),
                        ),
                      ),
                    )),
              ],
            )),
            Expanded(
                flex: 2,
                child: Container(
                  padding: const EdgeInsets.all(3),
                  decoration: const BoxDecoration(
                      color: Color(0xff292D36),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: buttons.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4),
                      itemBuilder: (context, index) {
                        switch (index) {
                          /// CLEAR BTN
                          case 0:
                            return CustomAppButton(
                              buttonTapped: () {
                                _mainPagePresenter.cleanState();
                              },
                              color: Color.fromARGB(255, 7, 255, 209),
                              textColor: Color.fromARGB(255, 33, 36, 42),
                              text: buttons[index],
                            );

                          /// DELETE BTN
                          case 1:
                            return CustomAppButton(
                                buttonTapped: () {
                                  _mainPagePresenter.deleteBtnAction();
                                },
                                color: Color.fromARGB(255, 7, 255, 209),
                                textColor: Color.fromARGB(255, 33, 36, 42),
                                text: buttons[index]);

                          /// EQUAL BTN
                          case 19:
                            return CustomAppButton(
                                buttonTapped: () {
                                  _mainPagePresenter.equalPressed();
                                },
                                color: Color.fromARGB(255, 7, 255, 209),
                                textColor: Color.fromARGB(255, 33, 36, 42),
                                text: buttons[index]);

                          default:
                            return CustomAppButton(
                              buttonTapped: () {
                                _mainPagePresenter.onBtnTapped(buttons, index);
                              },
                              color: isOperator(buttons[index])
                                  ? Colors.deepPurpleAccent
                                  : Color.fromARGB(255, 33, 36, 42),
                              textColor: context.colors.backgroundWhite,
                              text: buttons[index],
                            );
                        }
                      }),
                ))
          ],
        ),
      ),
    );
  }

  /// is Operator Check
  bool isOperator(String y) {
    if (y == "%" || y == "/" || y == "x" || y == "-" || y == "+" || y == "=") {
      return true;
    }
    return false;
  }
}

class CustomAppButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String text;
  final VoidCallback buttonTapped;

  const CustomAppButton({
    Key? key,
    required this.color,
    required this.textColor,
    required this.text,
    required this.buttonTapped,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: buttonTapped,
      child: Container(
        margin: const EdgeInsets.all(8),
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: textColor,
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
