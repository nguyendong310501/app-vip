import 'package:app_oke_vip/generated/l10n.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/app/bloc/app_state.dart';
import 'package:app_oke_vip/presentation/page/splash/splash_page.dart';
import 'package:app_oke_vip/resources/app_theme.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/app_helper.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/navigator_global_context_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:app_oke_vip/presentation/app/bloc/app_presenter.dart';
import 'package:intl/intl.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  final AppHelper appHelper = injector.get<AppHelper>();
  final AppPresenter _appPresenter = injector.get<AppPresenter>();

  @override
  void initState() {
    Intl.defaultLocale = _appPresenter.language;
    _appPresenter.init();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    appHelper.close();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    appHelper.addAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) => BlocConsumer<AppPresenter, AppState>(
        bloc: _appPresenter,
        listenWhen: (AppState previous, AppState current) =>
            previous.locale != current.locale,
        listener: (BuildContext context, AppState state) {},
        builder: (BuildContext context, AppState state) {
          return ScreenUtilInit(
            useInheritedMediaQuery: true,
            builder: (BuildContext context, Widget? child) {
              return MaterialApp(
                title: 'Dakcha employee',
                theme: getAppTheme(Brightness.light),
                localizationsDelegates:const [
                  S.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate
                ],
                darkTheme: getAppTheme(Brightness.light),
                home: const SplashPage(),
                navigatorKey:
                    injector.get<NavigatorGlobalContextHelper>().navigatorKey,
                debugShowCheckedModeBanner: false,
                supportedLocales: S.delegate.supportedLocales,
                locale: state.locale,
                useInheritedMediaQuery: false,
              );
            },
          );
        },
      );
}
