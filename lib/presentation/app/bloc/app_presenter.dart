import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'app_state.dart';

class AppPresenter extends Cubit<AppState> {
  AppPresenter({
    @visibleForTesting AppState? state,
  }) : super(state ?? AppState.initial());

  void init() {
    final String locale =  Locales.korea.value.code;
    emit(state.copyWith(locale: Locale(locale)));
  }

  void setLocale(String locale) {
    injector.get<AuthPreferencesRepository>().setLocale(locale);
    Intl.defaultLocale = locale;
    emit(state.copyWith(locale: Locale(locale)));
  }

  String get codeEnglish => Locales.english.value.code;

  String get codeVietnamese => Locales.vietnamese.value.code;

  String get language => state.locale.languageCode;

  bool get isCheckLanguageVN => state.locale.languageCode == codeVietnamese;
}
