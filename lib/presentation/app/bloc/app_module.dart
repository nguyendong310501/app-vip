import 'package:app_oke_vip/injection/injector.dart';

import 'app_presenter.dart';

class AppModule {
  static Future<void> inject() async {
    injector.registerLazySingleton<AppPresenter>(
      () => AppPresenter(

      ),
    );
  }
}
