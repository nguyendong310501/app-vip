// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `취소`
  String get text_common_cancel {
    return Intl.message(
      '취소',
      name: 'text_common_cancel',
      desc: '',
      args: [],
    );
  }

  /// `선택`
  String get text_common_choose {
    return Intl.message(
      '선택',
      name: 'text_common_choose',
      desc: '',
      args: [],
    );
  }

  /// `업로드`
  String get text_common_upload {
    return Intl.message(
      '업로드',
      name: 'text_common_upload',
      desc: '',
      args: [],
    );
  }

  /// `완료`
  String get text_common_dialog_ok {
    return Intl.message(
      '완료',
      name: 'text_common_dialog_ok',
      desc: '',
      args: [],
    );
  }

  /// `내용이 없습니다`
  String get text_common_text_empty {
    return Intl.message(
      '내용이 없습니다',
      name: 'text_common_text_empty',
      desc: '',
      args: [],
    );
  }

  /// `메시지`
  String get text_common_your_message {
    return Intl.message(
      '메시지',
      name: 'text_common_your_message',
      desc: '',
      args: [],
    );
  }

  /// `저장`
  String get text_common_save {
    return Intl.message(
      '저장',
      name: 'text_common_save',
      desc: '',
      args: [],
    );
  }

  /// `모두 선택`
  String get text_common_all {
    return Intl.message(
      '모두 선택',
      name: 'text_common_all',
      desc: '',
      args: [],
    );
  }

  /// `완료`
  String get text_common_done {
    return Intl.message(
      '완료',
      name: 'text_common_done',
      desc: '',
      args: [],
    );
  }

  /// `다시 시도해 주세요`
  String get text_common_error {
    return Intl.message(
      '다시 시도해 주세요',
      name: 'text_common_error',
      desc: '',
      args: [],
    );
  }

  /// `로그아웃`
  String get text_common_logout {
    return Intl.message(
      '로그아웃',
      name: 'text_common_logout',
      desc: '',
      args: [],
    );
  }

  /// `and`
  String get text_common_and {
    return Intl.message(
      'and',
      name: 'text_common_and',
      desc: '',
      args: [],
    );
  }

  /// `완성`
  String get text_common_complete {
    return Intl.message(
      '완성',
      name: 'text_common_complete',
      desc: '',
      args: [],
    );
  }

  /// `이벤트`
  String get text_common_event {
    return Intl.message(
      '이벤트',
      name: 'text_common_event',
      desc: '',
      args: [],
    );
  }

  /// `답장`
  String get text_common_reply {
    return Intl.message(
      '답장',
      name: 'text_common_reply',
      desc: '',
      args: [],
    );
  }

  /// `검색`
  String get text_common_search {
    return Intl.message(
      '검색',
      name: 'text_common_search',
      desc: '',
      args: [],
    );
  }

  /// `입력`
  String get text_common_enter {
    return Intl.message(
      '입력',
      name: 'text_common_enter',
      desc: '',
      args: [],
    );
  }

  /// `홈`
  String get text_bottom_bar_home {
    return Intl.message(
      '홈',
      name: 'text_bottom_bar_home',
      desc: '',
      args: [],
    );
  }

  /// `내역`
  String get text_bottom_bar_history {
    return Intl.message(
      '내역',
      name: 'text_bottom_bar_history',
      desc: '',
      args: [],
    );
  }

  /// `알림`
  String get text_bottom_bar_notification {
    return Intl.message(
      '알림',
      name: 'text_bottom_bar_notification',
      desc: '',
      args: [],
    );
  }

  /// `나의 정보`
  String get text_bottom_bar_info {
    return Intl.message(
      '나의 정보',
      name: 'text_bottom_bar_info',
      desc: '',
      args: [],
    );
  }

  /// `정보`
  String get text_bottom_bar_pub_call {
    return Intl.message(
      '정보',
      name: 'text_bottom_bar_pub_call',
      desc: '',
      args: [],
    );
  }

  /// `환영합니다`
  String get text_login_title {
    return Intl.message(
      '환영합니다',
      name: 'text_login_title',
      desc: '',
      args: [],
    );
  }

  /// `이메일 입력`
  String get text_login_your_email {
    return Intl.message(
      '이메일 입력',
      name: 'text_login_your_email',
      desc: '',
      args: [],
    );
  }

  /// `이메일 입력`
  String get text_login_your_password {
    return Intl.message(
      '이메일 입력',
      name: 'text_login_your_password',
      desc: '',
      args: [],
    );
  }

  /// `이메일`
  String get text_login_email {
    return Intl.message(
      '이메일',
      name: 'text_login_email',
      desc: '',
      args: [],
    );
  }

  /// `비밀번호`
  String get text_login_password {
    return Intl.message(
      '비밀번호',
      name: 'text_login_password',
      desc: '',
      args: [],
    );
  }

  /// `비밀번호를 잊으셨나요?`
  String get text_login_forgot_password {
    return Intl.message(
      '비밀번호를 잊으셨나요?',
      name: 'text_login_forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `로그인 유지하기`
  String get text_login_keep_login {
    return Intl.message(
      '로그인 유지하기',
      name: 'text_login_keep_login',
      desc: '',
      args: [],
    );
  }

  /// `로그인`
  String get text_login_login {
    return Intl.message(
      '로그인',
      name: 'text_login_login',
      desc: '',
      args: [],
    );
  }

  /// `app_oke_vip APP`
  String get text_splash {
    return Intl.message(
      'app_oke_vip APP',
      name: 'text_splash',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ko'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
