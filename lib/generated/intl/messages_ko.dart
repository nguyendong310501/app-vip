// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ko locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ko';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "text_bottom_bar_history": MessageLookupByLibrary.simpleMessage("내역"),
        "text_bottom_bar_home": MessageLookupByLibrary.simpleMessage("홈"),
        "text_bottom_bar_info": MessageLookupByLibrary.simpleMessage("나의 정보"),
        "text_bottom_bar_notification":
            MessageLookupByLibrary.simpleMessage("알림"),
        "text_bottom_bar_pub_call": MessageLookupByLibrary.simpleMessage("정보"),
        "text_common_all": MessageLookupByLibrary.simpleMessage("모두 선택"),
        "text_common_and": MessageLookupByLibrary.simpleMessage("and"),
        "text_common_cancel": MessageLookupByLibrary.simpleMessage("취소"),
        "text_common_choose": MessageLookupByLibrary.simpleMessage("선택"),
        "text_common_complete": MessageLookupByLibrary.simpleMessage("완성"),
        "text_common_dialog_ok": MessageLookupByLibrary.simpleMessage("완료"),
        "text_common_done": MessageLookupByLibrary.simpleMessage("완료"),
        "text_common_enter": MessageLookupByLibrary.simpleMessage("입력"),
        "text_common_error": MessageLookupByLibrary.simpleMessage("다시 시도해 주세요"),
        "text_common_event": MessageLookupByLibrary.simpleMessage("이벤트"),
        "text_common_logout": MessageLookupByLibrary.simpleMessage("로그아웃"),
        "text_common_reply": MessageLookupByLibrary.simpleMessage("답장"),
        "text_common_save": MessageLookupByLibrary.simpleMessage("저장"),
        "text_common_search": MessageLookupByLibrary.simpleMessage("검색"),
        "text_common_text_empty":
            MessageLookupByLibrary.simpleMessage("내용이 없습니다"),
        "text_common_upload": MessageLookupByLibrary.simpleMessage("업로드"),
        "text_common_your_message": MessageLookupByLibrary.simpleMessage("메시지"),
        "text_login_email": MessageLookupByLibrary.simpleMessage("이메일"),
        "text_login_forgot_password":
            MessageLookupByLibrary.simpleMessage("비밀번호를 잊으셨나요?"),
        "text_login_keep_login":
            MessageLookupByLibrary.simpleMessage("로그인 유지하기"),
        "text_login_login": MessageLookupByLibrary.simpleMessage("로그인"),
        "text_login_password": MessageLookupByLibrary.simpleMessage("비밀번호"),
        "text_login_title": MessageLookupByLibrary.simpleMessage("환영합니다"),
        "text_login_your_email": MessageLookupByLibrary.simpleMessage("이메일 입력"),
        "text_login_your_password":
            MessageLookupByLibrary.simpleMessage("이메일 입력"),
        "text_splash": MessageLookupByLibrary.simpleMessage("app_oke_vip APP")
      };
}
