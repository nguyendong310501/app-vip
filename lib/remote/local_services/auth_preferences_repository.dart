
abstract class AuthPreferencesRepository {
  Future<void> setAccessToken(String token);

  Future<void> setUserId(String id);

  String? getAccessToken();

  String? getUserId();

  Future<void> setRefreshToken(String token);

  String? getRefreshToken();

  Future<void> clearAll();

  Future<void> setLocale(String token);

  String? getLocale();

  String? getMail();

  String? getDeviceToken();

  void setMail(String email);

  Future<void> setDeviceToken(String deviceToken);
}
