import 'package:app_oke_vip/remote/network_services/body/base/api_response_model.dart';
import 'package:app_oke_vip/remote/network_services/body/upload_file/request/upload_file_request_model.dart';
import 'package:app_oke_vip/remote/network_services/body/upload_file/response/upload_file_response_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/device/device_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/info/response/user_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login/login_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login/login_output_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login_kakao/login_kakao_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/login_naver/login_naver_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/sender_code/sender_code_request_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/sender_code/verify_otp_request_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/sender_code/sign_in_response_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/sign_in/sign_in_input_model.dart';
import 'package:app_oke_vip/remote/network_services/body/user/sign_in/sing_in_expert_model.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:app_oke_vip/utilities/constants/api_constants.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi()
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @POST(ApiConstants.userLogin)
  Future<LoginResponseModel> login(
    @Body() LoginInputModel loginRequestModel,
  );

  @POST('/api/auth/signup')
  Future<LoginResponseModel> signIn(
    @Body() SignInInputModel loginRequestModel,
  );
  @POST('/api/auth/phone-number/send-otp')
  Future<void> sendOtp(
    @Body() PhoneRequestModel requestModel,
  );

  @POST('/api/auth/phone-number/verify-otp')
  Future<ApiResponseModel<SigninResponseModel>> verifyOtp(
    @Body() VerifyOtpRequestModel requestModel,
  );
  @POST('/api/expert')
  Future<void> registerExpert(
    @Body() SignInExpertInputModel requestModel,
  );

  @DELETE(ApiConstants.userLogout)
  Future<void> logout(
    @Queries() dynamic request,
  );

  @GET(ApiConstants.infoUser)
  Future<UserResponseModel> getInfoUsers();

  @POST(ApiConstants.upTokenDevices)
  Future<void> upTokenDevices(
    @Body() TokenRequestModel body,
  );

  @POST(ApiConstants.authLogout)
  Future<void> authLogout(
    @Body() TokenRequestModel body,
  );

  @POST(ApiConstants.loginKakao)
  Future<LoginResponseModel> loginWithKakao(
    @Body() LoginKakaoInputModel body,
  );
  @POST(ApiConstants.loginNaver)
  Future<LoginResponseModel> loginWithNaver(
    @Body() LoginNaverInputModel body,
  );
}
