// ignore_for_file: deprecated_member_use

import 'dart:io';
import 'package:app_oke_vip/presentation/page/authen/login/login_page.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:app_oke_vip/remote/network_services/body/auth/auth_token_model.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/navigator_global_context_helper.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/app/bloc/app_presenter.dart';
import 'package:app_oke_vip/utilities/constants/api_constants.dart';
import 'package:flutter/material.dart';

class AuthInterceptor extends InterceptorsWrapper {
  AuthInterceptor(
      this._authPreferencesRepository,
      this._dio,
      this._scaffoldGlobalContextHelper,
      this.navigationService);

  final AuthPreferencesRepository _authPreferencesRepository;
  final Dio _dio;

  _TokenRefreshStatus _tokenRefreshState = _TokenRefreshStatus.none;
  DioError? _tokenRefreshDioError;

  final ScaffoldGlobalContextHelper _scaffoldGlobalContextHelper;
  final NavigatorGlobalContextHelper navigationService;

  /// Whether request requires authentication or not.
  bool isAuthenticatedPath(RequestOptions options) =>
      !ApiConstants.nonAuthenticatedPaths.contains(options.path);

  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final String? accessToken = _authPreferencesRepository.getAccessToken();

    if (isAuthenticatedPath(options) && accessToken != null) {
      options.headers['Authorization'] = 'Bearer $accessToken';
      options.headers['Accept-Language'] =
          injector.get<AppPresenter>().language;
      print('onRequest');
    }

    handler.next(options);
  }

  void authenticated(DioError err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == HttpStatus.forbidden) {
      handleLogout();
      handler.reject(err);
    } else {
      _tokenRefreshState = _TokenRefreshStatus.none;
      handler.next(err);
    }
  }

  Future<void> handleLogout() async {
    await _authPreferencesRepository.clearAll();
    _scaffoldGlobalContextHelper.resetKey();
    handleLogoutSuccess();
  }

  void handleLogoutSuccess() {
    Navigator.of(navigationService.getCurrentContext).pushAndRemoveUntil(
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => const LoginPage(),
      ),
      ModalRoute.withName('/'),
    );
  }

  @override
  Future<void> onError(DioError err, ErrorInterceptorHandler handler) async {
    final RequestOptions requestOptions = err.requestOptions;
    final result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      print('No internet connection. Please check the connection again');
      return;
    }
    if (!isAuthenticatedPath(requestOptions) ||
        err.response?.statusCode != HttpStatus.unauthorized) {
      authenticated(err, handler);
      return;
    } else if (_tokenRefreshState.hasExpired) {
      handler.reject(err);
      return;
    } else if (_tokenRefreshState.hasFailed) {
      handler.reject(_tokenRefreshDioError!);
      return;
    } else {
      handler.next(err);
      return;
    }
  }

  Future _saveAuthTokensAndRetryCurrentRequest(
    AuthTokenModel authTokenModel,
    RequestOptions requestOptions,
    ErrorInterceptorHandler handler,
  ) async {
    await _authPreferencesRepository.setAccessToken(authTokenModel.accessToken);
    await _authPreferencesRepository
        .setRefreshToken(authTokenModel.refreshToken);

    // Unlock all requests.
    // _dio.unlock();

    // Update headers and retry the first failed request.
    requestOptions.headers['Authorization'] =
        'Bearer ${authTokenModel.accessToken}';
    handler.resolve(await _dio.fetch(requestOptions));
  }

  Future _clearUserSession() async {
    // Clear all requests in the queue and unlock client.
    // ..unlock();

    await _authPreferencesRepository.clearAll();
  }

  /// TODO: - Call API to refresh token
  Future<AuthTokenModel> _getNewAuthTokens(
          RequestOptions requestOptions) async =>
      const AuthTokenModel(
        accessToken: '',
        refreshToken: '',
        tokenType: '',
        expiresIn: 0,
      );
}

enum _TokenRefreshStatus {
  /// Default state. No ongoing token-refresh operation.
  none,

  /// Some other error while refreshing token.
  failed,

  /// Token refresh returned 401.
  expired,

  /// Token refresh is in progress.
  inProgress,
}

extension _TokenStatus on _TokenRefreshStatus {
  bool get hasExpired => this == _TokenRefreshStatus.expired;

  bool get isInProgress => this == _TokenRefreshStatus.inProgress;

  bool get hasFailed => this == _TokenRefreshStatus.failed;
}
