// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_file_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadFileResponseModel _$UploadFileResponseModelFromJson(
        Map<String, dynamic> json) =>
    UploadFileResponseModel(
      errorCode: json['error_code'] as int?,
      success: json['success'] as bool?,
      data: (json['data'] as List<dynamic>?)
          ?.map(
              (e) => DataFileResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UploadFileResponseModelToJson(
        UploadFileResponseModel instance) =>
    <String, dynamic>{
      'error_code': instance.errorCode,
      'success': instance.success,
      'data': instance.data,
    };

DeleteFileResponseModel _$DeleteFileResponseModelFromJson(
        Map<String, dynamic> json) =>
    DeleteFileResponseModel(
      errorCode: json['error_code'] as int?,
      success: json['success'] as bool?,
      data: json['data'] as bool?,
    );

Map<String, dynamic> _$DeleteFileResponseModelToJson(
        DeleteFileResponseModel instance) =>
    <String, dynamic>{
      'error_code': instance.errorCode,
      'success': instance.success,
      'data': instance.data,
    };

DataFileResponseModel _$DataFileResponseModelFromJson(
        Map<String, dynamic> json) =>
    DataFileResponseModel(
      url: json['url'] as String,
      signedUrl: json['signedUrl'] as String,
    );

Map<String, dynamic> _$DataFileResponseModelToJson(
        DataFileResponseModel instance) =>
    <String, dynamic>{
      'url': instance.url,
      'signedUrl': instance.signedUrl,
    };
