import 'package:app_oke_vip/remote/network_services/body/base/api_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'upload_file_response_model.g.dart';

@JsonSerializable()
class UploadFileResponseModel
    extends ApiResponseModel<List<DataFileResponseModel>?> {
  const UploadFileResponseModel({
    int? errorCode,
    bool? success,
    List<DataFileResponseModel>? data,
  }) : super(
          errorCode: errorCode,
          success: success,
          data: data,
        );

  factory UploadFileResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UploadFileResponseModelFromJson(json);

}

@JsonSerializable()
class DeleteFileResponseModel extends ApiResponseModel<bool?> {
  const DeleteFileResponseModel({
    int? errorCode,
    bool? success,
    bool? data,
  }) : super(
          errorCode: errorCode,
          success: success,
          data: data,
        );

  factory DeleteFileResponseModel.fromJson(Map<String, dynamic> json) =>
      _$DeleteFileResponseModelFromJson(json);
}

@JsonSerializable()
class DataFileResponseModel {
  const DataFileResponseModel({required this.url, required this.signedUrl});

  factory DataFileResponseModel.fromJson(Map<String, dynamic> json) =>
      _$DataFileResponseModelFromJson(json);

  @JsonKey(name: "url")
  final String url;
  @JsonKey(name: "signedUrl")
  final String signedUrl;
  Map<String, dynamic> toJson() => _$DataFileResponseModelToJson(this);


}
