// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_file_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadFileRequestModel _$UploadFileRequestModelFromJson(
        Map<String, dynamic> json) =>
    UploadFileRequestModel(
      fileName:
          (json['fileNames'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$UploadFileRequestModelToJson(
        UploadFileRequestModel instance) =>
    <String, dynamic>{
      'fileNames': instance.fileName,
    };
