import 'package:json_annotation/json_annotation.dart';

part 'upload_file_request_model.g.dart';

@JsonSerializable()
class UploadFileRequestModel {
  const UploadFileRequestModel({required this.fileName});

  factory UploadFileRequestModel.fromJson(Map<String, dynamic> json) =>
      _$UploadFileRequestModelFromJson(json);

  @JsonKey(name: 'fileNames')
  final List<String> fileName;

  Map<String, dynamic> toJson() => _$UploadFileRequestModelToJson(this);
}
