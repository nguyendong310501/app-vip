// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_naver_input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginNaverInputModel _$LoginNaverInputModelFromJson(
        Map<String, dynamic> json) =>
    LoginNaverInputModel(
      accessToken: json['access_token'] as String,
    );

Map<String, dynamic> _$LoginNaverInputModelToJson(
        LoginNaverInputModel instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
    };
