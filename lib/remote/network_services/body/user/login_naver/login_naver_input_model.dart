import 'package:json_annotation/json_annotation.dart';

part 'login_naver_input_model.g.dart';

@JsonSerializable()
class LoginNaverInputModel {
  const LoginNaverInputModel(
      {required this.accessToken});

  factory LoginNaverInputModel.fromJson(Map<String, dynamic> json) =>
      _$LoginNaverInputModelFromJson(json);

  @JsonKey(name: 'access_token')
  final String accessToken;

  Map<String, dynamic> toJson() => _$LoginNaverInputModelToJson(this);
}
