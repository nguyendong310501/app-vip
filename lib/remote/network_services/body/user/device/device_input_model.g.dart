// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TokenRequestModel _$TokenRequestModelFromJson(Map<String, dynamic> json) =>
    TokenRequestModel(
      token: json['device_token'] as String,
    );

Map<String, dynamic> _$TokenRequestModelToJson(TokenRequestModel instance) =>
    <String, dynamic>{
      'device_token': instance.token,
    };
