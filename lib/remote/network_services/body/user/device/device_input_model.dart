import 'package:json_annotation/json_annotation.dart';

part 'device_input_model.g.dart';

@JsonSerializable()
class TokenRequestModel {
  const TokenRequestModel({
    required this.token,
  });

  factory TokenRequestModel.fromJson(Map<String, dynamic> json) =>
      _$TokenRequestModelFromJson(json);

  @JsonKey(name: 'device_token')
  final String token;

  Map<String, dynamic> toJson() => _$TokenRequestModelToJson(this);
}
