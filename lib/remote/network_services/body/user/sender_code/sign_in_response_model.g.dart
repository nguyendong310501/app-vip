// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_in_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SigninResponseModel _$SigninResponseModelFromJson(Map<String, dynamic> json) =>
    SigninResponseModel()
      ..token = json['registration_token'] as String?
      ..expireAt = json['expire_at'] as String?;

Map<String, dynamic> _$SigninResponseModelToJson(
        SigninResponseModel instance) =>
    <String, dynamic>{
      'registration_token': instance.token,
      'expire_at': instance.expireAt,
    };
