import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_in_response_model.g.dart';


@JsonSerializable()
class SigninResponseModel {


  factory SigninResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SigninResponseModelFromJson(json);

  @JsonKey(name: 'registration_token')
   String? token;

  @JsonKey(name: 'expire_at')
   String? expireAt;

   SigninResponseModel();
  Map<String, dynamic> toJson() => _$SigninResponseModelToJson(this);

}