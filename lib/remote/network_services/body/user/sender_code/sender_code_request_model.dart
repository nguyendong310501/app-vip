import 'package:json_annotation/json_annotation.dart';

part 'sender_code_request_model.g.dart';

@JsonSerializable()
class PhoneRequestModel {


  factory PhoneRequestModel.fromJson(Map<String, dynamic> json) =>
      _$PhoneRequestModelFromJson(json);

  @JsonKey(name: 'phone_number')
   String? phoneNumber;
  PhoneRequestModel();

  Map<String, dynamic> toJson() => _$PhoneRequestModelToJson(this);
}
