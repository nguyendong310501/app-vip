// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sender_code_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneRequestModel _$PhoneRequestModelFromJson(Map<String, dynamic> json) =>
    PhoneRequestModel()..phoneNumber = json['phone_number'] as String?;

Map<String, dynamic> _$PhoneRequestModelToJson(PhoneRequestModel instance) =>
    <String, dynamic>{
      'phone_number': instance.phoneNumber,
    };
