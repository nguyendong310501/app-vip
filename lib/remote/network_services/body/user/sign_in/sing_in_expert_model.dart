import 'package:json_annotation/json_annotation.dart';

part 'sing_in_expert_model.g.dart';

@JsonSerializable()
class SignInExpertInputModel {

  @JsonKey(name: 'services')
  List<String>? services;

  @JsonKey(name: 'activity_area')
  String? activityArea;

  @JsonKey(name: 'gender')
  String? gender;

  @JsonKey(name: 'additional_activity_areas')
  List<String>? additionalActivityAreas;

  @JsonKey(name: 'registration_token')
  String? registrationToken;

  @JsonKey(name: 'phone_number')
  String? phoneNumber;

  SignInExpertInputModel();

  factory SignInExpertInputModel.fromJson(Map<String, dynamic> json) =>
      _$SignInExpertInputModelFromJson(json);

  Map<String, dynamic> toJson() => _$SignInExpertInputModelToJson(this);
}
