// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_in_input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignInInputModel _$SignInInputModelFromJson(Map<String, dynamic> json) =>
    SignInInputModel(
      email: json['email'] as String,
      password: json['password'] as String,
      fullName: json['full_name'] as String,
    );

Map<String, dynamic> _$SignInInputModelToJson(SignInInputModel instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'full_name': instance.fullName,
    };
