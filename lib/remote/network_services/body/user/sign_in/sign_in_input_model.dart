import 'package:json_annotation/json_annotation.dart';

part 'sign_in_input_model.g.dart';

@JsonSerializable()
class SignInInputModel {
  const SignInInputModel({required this.email, required this.password,required this.fullName});

  factory SignInInputModel.fromJson(Map<String, dynamic> json) =>
      _$SignInInputModelFromJson(json);

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'password')
  final String password;

 @JsonKey(name: 'full_name')
  final String fullName;

  Map<String, dynamic> toJson() => _$SignInInputModelToJson(this);
}
