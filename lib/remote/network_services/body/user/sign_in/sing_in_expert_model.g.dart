// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sing_in_expert_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignInExpertInputModel _$SignInExpertInputModelFromJson(
        Map<String, dynamic> json) =>
    SignInExpertInputModel()
      ..services =
          (json['services'] as List<dynamic>?)?.map((e) => e as String).toList()
      ..activityArea = json['activity_area'] as String?
      ..gender = json['gender'] as String?
      ..additionalActivityAreas =
          (json['additional_activity_areas'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList()
      ..registrationToken = json['registration_token'] as String?
      ..phoneNumber = json['phone_number'] as String?;

Map<String, dynamic> _$SignInExpertInputModelToJson(
        SignInExpertInputModel instance) =>
    <String, dynamic>{
      'services': instance.services,
      'activity_area': instance.activityArea,
      'gender': instance.gender,
      'additional_activity_areas': instance.additionalActivityAreas,
      'registration_token': instance.registrationToken,
      'phone_number': instance.phoneNumber,
    };
