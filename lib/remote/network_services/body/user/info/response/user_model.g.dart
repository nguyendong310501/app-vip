// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponseModel _$UserResponseModelFromJson(Map<String, dynamic> json) =>
    UserResponseModel(
      errorCode: json['error_code'] as int?,
      success: json['success'] as bool?,
      data: json['data'] == null
          ? null
          : UserModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserResponseModelToJson(UserResponseModel instance) =>
    <String, dynamic>{
      'error_code': instance.errorCode,
      'success': instance.success,
      'data': instance.data,
    };

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      json['avatar'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
      deletedAt: json['deleted_at'] as String?,
      id: json['id'] as String?,
      fullName: json['full_name'] as String?,
      phoneNumber: json['phone_number'] as String?,
      isDeleted: json['is_deleted'] as bool?,
      provider: json['provider'] as String?,
      resetPasswordAt: json['reset_password_at'] as String?,
      lockedAt: json['locked_at'] as String?,
      userRoles: (json['user_roles'] as List<dynamic>?)
          ?.map((e) => RoleModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      email: json['email'] as String?,
      status: json['status'] as String?,
      externalUserId: json['external_user_id'] as String?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'is_deleted': instance.isDeleted,
      'deleted_at': instance.deletedAt,
      'full_name': instance.fullName,
      'email': instance.email,
      'phone_number': instance.phoneNumber,
      'avatar': instance.avatar,
      'status': instance.status,
      'provider': instance.provider,
      'reset_password_at': instance.resetPasswordAt,
      'external_user_id': instance.externalUserId,
      'locked_at': instance.lockedAt,
      'user_roles': instance.userRoles,
    };

RoleModel _$RoleModelFromJson(Map<String, dynamic> json) => RoleModel(
      id: json['id'] as String?,
      userId: json['user_id'] as String?,
      roleName: json['role_name'] as String?,
      isDeleted: json['is_deleted'] as bool?,
      franchiseId: json['franchise_id'] as String?,
      status: json['status'] as String?,
      createdAt: json['created_at'] as String?,
      deletedAt: json['deleted_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$RoleModelToJson(RoleModel instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'is_deleted': instance.isDeleted,
      'deleted_at': instance.deletedAt,
      'role_name': instance.roleName,
      'status': instance.status,
      'franchise_id': instance.franchiseId,
    };
