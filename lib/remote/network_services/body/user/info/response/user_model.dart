import 'package:app_oke_vip/remote/network_services/body/base/api_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserResponseModel extends ApiResponseModel<UserModel?> {
  const UserResponseModel({
    int? errorCode,
    bool? success,
    String? message,
    UserModel? data,
  }) : super(
          errorCode: errorCode,
          success: success,
          data: data,
        );

  factory UserResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UserResponseModelFromJson(json);
}

@JsonSerializable()
class UserModel {
  const UserModel(this.avatar, 
      {required this.createdAt,
      required this.updatedAt,
      required this.deletedAt,
      required this.id,
      required this.fullName,
      required this.phoneNumber,
      required this.isDeleted,
      required this.provider,
      required this.resetPasswordAt,
      required this.lockedAt,
      required this.userRoles,
      required this.email,
      required this.status,
      required this.externalUserId
});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  @JsonKey(name: 'id')
  final String? id;

  @JsonKey(name: 'created_at')
  final String? createdAt;

  @JsonKey(name: 'updated_at')
  final String? updatedAt;

  @JsonKey(name: 'is_deleted')
  final bool? isDeleted;

  @JsonKey(name: 'deleted_at')
  final String? deletedAt;

  @JsonKey(name: 'full_name')
  final String? fullName;

  @JsonKey(name: 'email')
  final String? email;

  @JsonKey(name: 'phone_number')
  final String? phoneNumber;

  @JsonKey(name: 'avatar')
  final String? avatar;

  @JsonKey(name: 'status')
  final String? status;

  @JsonKey(name: 'provider')
  final String? provider;

  @JsonKey(name: 'reset_password_at')
  final String? resetPasswordAt;

  @JsonKey(name: 'external_user_id')
  final String? externalUserId;

  @JsonKey(name: 'locked_at')
  final String? lockedAt;

  @JsonKey(name: 'user_roles')
  final List<RoleModel>? userRoles;


  Map<String, dynamic> toJson() => _$UserModelToJson(this);

}

@JsonSerializable()
class RoleModel {
  @JsonKey(name: 'id')
  final String? id;
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @JsonKey(name: 'is_deleted')
  final bool? isDeleted;
  @JsonKey(name: 'deleted_at')
  final String? deletedAt;
  @JsonKey(name: 'role_name')
  final String? roleName;
  @JsonKey(name: 'status')
  final String? status;
  @JsonKey(name: 'franchise_id')
  final String? franchiseId;

  RoleModel({
    required this.id,
    required this.userId,
    required this.roleName,
    required this.isDeleted,
    required this.franchiseId,
    required this.status,
    required this.createdAt,
    required this.deletedAt,
    required this.updatedAt,
  });

  factory RoleModel.fromJson(Map<String, dynamic> json) =>
      _$RoleModelFromJson(json);

  Map<String, dynamic> toJson() => _$RoleModelToJson(this);


}
