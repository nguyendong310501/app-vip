// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_output_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponseModel _$LoginResponseModelFromJson(Map<String, dynamic> json) =>
    LoginResponseModel(
      errorCode: json['error_code'] as int?,
      success: json['success'] as bool?,
      data: json['data'] == null
          ? null
          : LoginOutputModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseModelToJson(LoginResponseModel instance) =>
    <String, dynamic>{
      'error_code': instance.errorCode,
      'success': instance.success,
      'data': instance.data,
    };

LoginOutputModel _$LoginOutputModelFromJson(Map<String, dynamic> json) =>
    LoginOutputModel(
      expireAt: json['expire_at'] as String,
      status: json['status'] as String,
      token: json['access_token'] as String,
      userId: json['user_id'] as String,
    );

Map<String, dynamic> _$LoginOutputModelToJson(LoginOutputModel instance) =>
    <String, dynamic>{
      'access_token': instance.token,
      'user_id': instance.userId,
      'status': instance.status,
      'expire_at': instance.expireAt,
    };
