import 'package:app_oke_vip/remote/network_services/body/base/api_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_output_model.g.dart';

@JsonSerializable()
class LoginResponseModel extends ApiResponseModel<LoginOutputModel?> {
  const LoginResponseModel({
    int? errorCode,
    bool? success,
    String? message,
    LoginOutputModel? data,
  }) : super(
          errorCode: errorCode,
          success: success,
          data: data,
        );

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseModelFromJson(json);
}

@JsonSerializable()
class LoginOutputModel {
  const LoginOutputModel(
      {required this.expireAt,
      required this.status,
      required this.token,
      required this.userId});

  factory LoginOutputModel.fromJson(Map<String, dynamic> json) =>
      _$LoginOutputModelFromJson(json);

  @JsonKey(name: 'access_token')
  final String token;

  @JsonKey(name: 'user_id')
  final String userId;

  @JsonKey(name: 'status')
  final String status;

  @JsonKey(name: 'expire_at')
  final String expireAt;

  Map<String, dynamic> toJson() => _$LoginOutputModelToJson(this);

}
