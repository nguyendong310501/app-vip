// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInputModel _$LoginInputModelFromJson(Map<String, dynamic> json) =>
    LoginInputModel(
      email: json['email'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$LoginInputModelToJson(LoginInputModel instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };
