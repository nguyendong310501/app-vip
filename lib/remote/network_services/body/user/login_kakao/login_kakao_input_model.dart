import 'package:json_annotation/json_annotation.dart';

part 'login_kakao_input_model.g.dart';

@JsonSerializable()
class LoginKakaoInputModel {
  const LoginKakaoInputModel({required this.accessToken, required this.idToken});

  factory LoginKakaoInputModel.fromJson(Map<String, dynamic> json) =>
      _$LoginKakaoInputModelFromJson(json);

  @JsonKey(name: 'access_token')
  final String accessToken;

  @JsonKey(name: 'id_token')
  final String idToken;

  Map<String, dynamic> toJson() => _$LoginKakaoInputModelToJson(this);
}
