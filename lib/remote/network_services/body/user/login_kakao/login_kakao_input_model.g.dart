// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_kakao_input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginKakaoInputModel _$LoginKakaoInputModelFromJson(
        Map<String, dynamic> json) =>
    LoginKakaoInputModel(
      accessToken: json['access_token'] as String,
      idToken: json['id_token'] as String,
    );

Map<String, dynamic> _$LoginKakaoInputModelToJson(
        LoginKakaoInputModel instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'id_token': instance.idToken,
    };
