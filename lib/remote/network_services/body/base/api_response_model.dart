import 'package:json_annotation/json_annotation.dart';


part 'api_response_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ApiResponseModel<T> {
  const ApiResponseModel({
    this.errorCode,
    this.success,
    this.data,
  });

  factory ApiResponseModel.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$ApiResponseModelFromJson<T>(json, fromJsonT);

  @JsonKey(name: 'error_code')
  final int? errorCode;

  @JsonKey(name: 'success')
  final bool? success;

  @JsonKey(name: 'data')
  final T? data;

  Map<String, dynamic> toJson(
    ApiResponseModel<T> instance,
    Object? Function(T value) toJsonT,
  ) =>
      _$ApiResponseModelToJson(instance, toJsonT);

  ApiResponse<T> toApiResponse() => ApiResponse<T>(
        errorCode: errorCode,
        success: success,
      );
}

class ApiResponse<T> {
  const ApiResponse({this.errorCode, this.success, this.data});

  final int? errorCode;

  final bool? success;

  final T? data;
}

enum ProcessStatus {
  initialized,
  loading,
  success,
  faluire;

  bool get isLoading => this == ProcessStatus.loading;

  bool get isSuccess => this == ProcessStatus.success;

  bool get isFailure => this == ProcessStatus.faluire;

  bool get isInitialized => this == ProcessStatus.initialized;
}
