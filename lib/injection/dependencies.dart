import 'package:app_oke_vip/injection/app_modules.dart';
import 'package:app_oke_vip/injection/injector.dart';
import 'package:app_oke_vip/presentation/app/bloc/app_module.dart';
 import 'package:app_oke_vip/presentation/page/main/bloc/main_page_module.dart';
import 'package:app_oke_vip/components/calendart_horizontal/bloc/calendart_horizontal_module.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/app_flavor_helper.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/navigator_global_context_helper.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';

class DependencyManager {
  static Future<void> inject(AppFlavor appFlavor) async {

    // App Flavor
    injector.registerLazySingleton<AppFlavor>(() => appFlavor);

    // Inject get current context
    injector.registerLazySingleton<NavigatorGlobalContextHelper>(
      () => NavigatorGlobalContextHelper(),
    );

    injector.registerLazySingleton<ScaffoldGlobalContextHelper>(
      () => ScaffoldGlobalContextHelper(),
    );

    // App modules
    await AppModules.inject();

    // Feature modules
    await AppModule.inject();

    await CalendartHorizontalModule.inject();

    await MainPageModule.inject();


  }
}
