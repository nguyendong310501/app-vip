import 'package:app_oke_vip/remote/local_services/auth_preferences_repository.dart';
import 'package:app_oke_vip/remote/local_services/auth_preferences_repository_implement.dart';
import 'package:app_oke_vip/remote/network_services/api_client/api_client.dart';
import 'package:app_oke_vip/remote/network_services/interceptor/auth_interceptor.dart';
import 'package:app_oke_vip/remote/network_services/interceptor/logger_interceptor.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/navigator_global_context_helper.dart';
import 'package:app_oke_vip/utilities/helpers/app_helper/scaffold_global_context_helper.dart';
import 'package:dio/dio.dart';
import 'package:app_oke_vip/utilities/constants/api_constants.dart';
import 'package:app_oke_vip/utilities/helpers/fcm_push_notification/fcm_push_notification.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './injector.dart';
import '../utilities/helpers/app_helper/app_flavor_helper.dart';
import '../utilities/helpers/app_helper/app_helper.dart';
import '../utilities/helpers/dialog_helper/loading_full_screen_helper.dart';
import '../utilities/helpers/error_helper/error_helper.dart';
import '../utilities/helpers/logger_helper/logger_helper.dart';
import '../utilities/helpers/validator_helper/validator_helper.dart';

class AppModules {
  static Future<void> inject() async {
    // Share Preferences
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();

    injector.registerLazySingleton<AuthPreferencesRepository>(
      () => AuthPreferencesRepositoryImplement(sharedPreferences),
    );

    // Helpers
    injector.registerLazySingleton<LoggerHelper>(
      () => LoggerHelper(
        isEnabled: injector.get<AppFlavor>().isDevelopment,
      ),
    );

    injector.registerLazySingleton<ErrorHelper>(
      () => ErrorHelper(),
    );

    injector.registerLazySingleton<ValidatorHelper>(
      () => ValidatorHelper(),
    );

    injector.registerLazySingleton<AppHelper>(
      () => AppHelper(),
    );

    injector.registerLazySingleton<LoadingFullScreenHelper>(
      () => LoadingFullScreenHelper(),
    );

    // Http Client
    injector.registerLazySingleton<Dio>(() {
      final Dio dio = Dio();
      dio.options.baseUrl = ApiConstants.apiBaseUrl;
      dio.options.connectTimeout = const Duration(milliseconds: 6000);
      dio.options.receiveTimeout = const Duration(milliseconds: 6000);

      dio.interceptors.add(AuthInterceptor(
          injector.get<AuthPreferencesRepository>(),
          dio,
          injector.get<ScaffoldGlobalContextHelper>(),
          injector.get<NavigatorGlobalContextHelper>()));
      if (injector.get<AppFlavor>().isDevelopment) {
        dio.interceptors.add(LoggerInterceptor());
      }

      return dio;
    });

    injector.registerLazySingleton<ApiClient>(
      () => ApiClient(
        injector.get<Dio>(),
        baseUrl: ApiConstants.apiBaseUrl,
      ),
    );

    // Use cases
    injector.registerSingleton<FCMPushNotification>(
      FCMPushNotification(delegate: FCMPushNotificationAction()),
    );
  }
}
