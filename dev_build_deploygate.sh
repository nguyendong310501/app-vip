export CURRENT_DATE=$(date +"%Y/%m/%d %H:%M")
export CURRENT_VERSION_NAME="1.1.9"
export CURRENT_VERSION_BUILD="9"
export OWNER_NAME="phuquy2114"
export API_TOKEN="5f60b52cac1369a03affcdd1bd462c8b7b839016"

 flutter build apk --no-shrink --build-name=$CURRENT_VERSION_NAME --build-number=$CURRENT_VERSION_BUILD --target lib/main.dart
 flutter build appbundle --no-shrink --build-name=$CURRENT_VERSION_NAME --build-number=$CURRENT_VERSION_BUILD --target lib/main.dart

# iOS
#fvm flutter build ipa --obfuscate --split-debug-info=release/build/ios/ipa/  --build-name=$CURRENT_VERSION_NAME --build-number=1  --target lib/main.dart


MESSAGE=" 🚀🚀🚀 [DEV] Start Deployment =>  Your application is deploying... Please waiting 30m ....
  Commit URL: Branch master:
    Environment: DEV  - ${CURRENT_VERSION_NAME}
    Date: ${CURRENT_DATE}"

curl \
  --url "https://deploygate.com/api/users/${OWNER_NAME}/apps" \
  -H "Authorization: Bearer ${API_TOKEN}" \
  -X POST \
  -F "file=@./build/app/outputs/flutter-apk/app-release.apk" \
  --form-string "message=${MESSAGE}"
